/**
 * Created by Alexander on 7/9/14.
 */
define(['utils', 'ws'], function (utils, wsConnect) {

    var player = null;
    var player2 = null;

    var mob1 = null;
    var mob2 = null;
    var mob3 = null;

    var data = null;
    var expect = chai.expect;

    var speed = null;
    var map = [
        ["#", "#", "#", "#", "#", "#", "#"],
        ["#", ".", ".", ".", ".", ".", "#"],
        ["#", ".", ".", ".", ".", ".", "#"],
        ["#", ".", ".", ".", ".", ".", "#"],
        ["#", "#", "#", "#", "#", "#", "#"]
    ]

    function Test() {
        describe('Mobs', function () {

            it('Register player 1', function (done) {
                this.timeout(10000);
                player = utils.createPlayer();
                player.x =  1.5;
                player.y =  1.5;
                player.speed = 0.3;
                utils.register(player, expect,
                    function () {utils.login(player, expect,
                        function () {utils.startTest(player, expect,
                            function() {utils.setMap(player, map, expect,
                                function() {utils.setSpeed(player,
                                    function() {utils.setPosition(player, expect,
                                        done
                                    )}
                                )}
                            )}
                        )}
                    )}
                );
            });

//
//            it('Player 2 [1.5, 1.5] should login in free place, when player 1 stay in this location', function (done) {
//                this.timeout(10000);
//
//                player = utils.createPlayer();
//                player.x =  1.5;
//                player.y =  1.5;
//
//                player2 = utils.createPlayer();
//                player2.x =  1.5;
//                player2.y =  1.5;
//                player2.speed = 0.3;
//
//                utils.register(player2, expect,
//                    function () {utils.login(player2, expect,
//                        function () {utils.startTest(player2, expect,
//                            function() {utils.setMap(player2, map, expect,
//                                function() {utils.setSpeed(player2,
//                                    function () {utils.setPosition(player2, expect,
//                                        function() {utils.wait(player2, 3,
//                                            function (){
//                                                player2.ws.sendJSON({action: "logout", sid: player2.sid});
//                                                utils.register(player, expect,
//                                                    function () {utils.login(player, expect,
//                                                        function() {utils.setPosition(player, expect,
//                                                            function () {utils.login(player2, expect,
//                                                                function () {
//                                                                    player2.ws.onmessage = function (e) {
//                                                                        var data = JSON.parse(e.data);
//                                                                        if (data['action'] == "look") {
//                                                                            player2.ws.onmessage = undefined;
//                                                                            player2.ws.sendJSON({action: "logout", sid: player2.sid});
//                                                                            console.log("EX_PLAYER_2");
//                                                                            console.log(data);
//                                                                            expect(Math.abs(data.x - player.x) > 0.9 || Math.abs(data.y - player.y) > 0.9).to.be.ok;
//                                                                            done();
//                                                                        }
//                                                                    }
//                                                                    player2.ws.sendJSON({ action: "look", sid: player2.sid});
//                                                                }
//                                                            )}
//                                                        )}
//                                                    )}
//                                                )
//                                            }
//                                        )}
//                                    )}
//                                )}
//                            )}
//                        )}
//                    )}
//                );
//            });

            it('Mob [5.5, 3.5, canMove = true, canBlow = false] must come to player', function (done) {
                this.timeout(100000);
                mob1 = {
                    x: 5.5,
                    y: 3.5,
                    stats: {HP: 50, MAX_HP: 50},
                    flags: ["CAN_MOVE", "HATE_METAL", "HATE_PLAYER"],
                    race: "ORC",
                    damage: "5d4"
                }
                utils.putMob(player, mob1, expect,
                    function(){utils.wait(player, 150,
                        function(){utils.checkPositionWithExamine(player, mob1, player.x, player.y, 1.1, expect, done)}
                    )}
                );
            });

            it('Player successfully kill mob [50 * hit]', function (done) {
                this.timeout(400000);
                utils.fightWithMob(player, mob1, 50,
                    function(){utils.checkMob(player, mob1, 'badId', expect, done)}
                );
            });

            it('Mob [5.5, 3.5, canMove = false, canBlow = false] must stay', function (done) {
                this.timeout(10000);
                mob1 = {
                    x: 5.5,
                    y: 3.5,
                    stats: {HP: 50, MAX_HP: 50},
                    flags: ["HATE_METAL", "HATE_PLAYER"],
                    race: "ORC",
                    damage: "5d4"
                }
                utils.putMob(player, mob1, expect,
                    function(){utils.wait(player, 5,
                        function(){utils.checkPositionWithExamine(player, mob1, 5.5, 3.5, 0.001, expect, done)}
                    )}
                );
            });

            it('Player must successfully abut the mob', function (done) {
                this.timeout(400000);
                utils.makeStepsTo(player, "south", 40,
                        function(){utils.makeStepsTo(player, "east", 50,
                            function(){utils.checkPosition(player, 4.5, 3.5, 0.2, expect, done)}
                        )}
                );
            });

            it('Player successfully kill mob [50 * hit]', function (done) {
                this.timeout(400000);
                utils.fightWithMob(player, mob1, 150,
                    function(){utils.checkMob(player, mob1, 'badId', expect, done)}
                );
            });

            it('Mob_1 and mob_2 should approach each other', function (done) {
                this.timeout(500000);
                player.x = 1.5;
                player.y = 1.5;
                mob1 = {
                    x: 1.5,
                    y: 3.5,
                    stats: {HP: 50, MAX_HP: 50},
                    flags: ["CAN_MOVE", "HATE_METAL"],
                    race: "ORC",
                    damage: "5d4"
                };

                mob2 = {
                    x: 5.5,
                    y: 1.5,
                    stats: {HP: 50, MAX_HP: 50},
                    flags: ["CAN_MOVE", "HATE_ORC"],
                    race: "METAL",
                    damage: "5d4"
                };

                utils.setPosition(player, expect,
                    function() {utils.putMob(player, mob1, expect,
                        function() {utils.putMob(player, mob2, expect,
                            function(){utils.wait(player, 150,
                                function(){utils.examine(player, mob1, expect,
                                    function(){utils.checkPositionWithExamine(player, mob2, mob1.x, mob1.y, 1.1, expect, done)}
                                )}
                            )}
                        )}
                    )}
                );
            });

            it('Mob_3 [3.5, 3.5, canMove = true, canBlow = true] must kill mob_1 & mob_2', function (done) {
                this.timeout(100000);
                mob3 = {
                    x: 2.5,
                    y: 1.5,
                    stats: {HP: 50, MAX_HP: 50},
                    flags: ["CAN_MOVE", "CAN_BLOW", "HATE_ORC", "HATE_METAL"],
                    race: "ANIMAL",
                    damage: "50d40"
                };
                utils.examine(player, mob1, expect,
                    function(){utils.examine(player, mob2, expect,
                        function(){utils.examine(player, player, expect,
                            function(){utils.putMob(player, mob3, expect,

                                function(){utils.wait(player, 150,
                                    function(){utils.checkMob(player, mob1, 'badId', expect,
                                        function(){utils.checkMob(player, mob2, 'badId', expect, done)}
                                    )}
                                )}
                            )}
                        )}
                    )}
                );
            });

            it('Should fail putMod [badPlaning]', function (done) {
                this.timeout(400000);
                utils.checkMob(player, mob3, 'ok', expect,
                    function() {utils.putMob(player, {x: mob3.x, y: mob3.y, flags: [], race: "METAL", damage: "4d4"}, expect, done)}
                );
            });


            it('End test', function (done) {
                player.ws.sendJSON({action: 'stopTesting', sid: player.sid});
                player.ws.sendJSON({action: "logout", sid: player.sid});
                done();
            });
        });
    }

    return {
        Test: Test
    }
});
