/**
 * Created by Alexander on 5/31/14.
 */
define(['utils'], function (utils) {

    var ws = null;
    var player = null;
    var sid = null;
    var data = null;
    var tick = null;
    var expect = chai.expect;

    var pv = 0.567;
    var st = 0.123;
    var tps = 60;
    var rc = 8;
    var cc = 12;
    var pur = 1.5;

    var map = [
        ["#", "#", "#", "#", "#", "#", "#"],
        ["#", ".", ".", "#", ".", ".", "#"],
        ["#", ".", ".", ".", ".", ".", "#"],
        ["#", ".", ".", "#", ".", ".", "#"],
        ["#", "#", "#", "#", "#", "#", "#"]
    ]

    function Test() {
        describe('Prepare server', function () {

            it('Register player 1', function (done) {
                this.timeout(10000);
                player = utils.createPlayer();
                utils.register(player, expect,
                    function () {utils.login(player, expect, done)}
                );
            });

            it('Should successfully get dictionary', function (done) {
                player.ws.onmessage = function (e) {
                    var data = JSON.parse(e.data);
                    if (data['action'] == "getDictionary") {
                        expect(data.dictionary).to.have.property('.', 'grass');
                        expect(data.dictionary).to.have.property('#', 'wall');
                        player.ws.onmessage = function(e) {console.log(JSON.parse(e.data))};
                        done();
                    }
                }
                player.ws.sendJSON({action: "getDictionary", sid: player.sid});
            });

            it('Should successfully get tick', function (done) {
                player.ws.onmessage = function (messeage) {
                    console.log(JSON.parse(messeage.data));
                    data = JSON.parse(messeage.data);
                    if (data['tick']) {
                        player.ws.onmessage = undefined;
                        tick = data['tick'];
                        done();
                    }
                }
            });

            it('Should successfully update tick', function (done) {
                player.ws.onmessage = function (messeage) {
                    data = JSON.parse(messeage.data);
                    if (data['tick']) {
                        player.ws.onmessage = undefined;
                        expect(data['tick']).to.above(tick);
                        done();
                    }
                }
            });

            it('Should fail get dictionary by badSid', function (done) {
                player.ws.onmessage = function (e) {
                    var data = JSON.parse(e.data);
                    if (data['action'] == 'getDictionary') {
                        expect(data['result']).to.equal('badSid');
                        player.ws.onmessage = undefined;
                        done();
                    }
                }
                player.ws.sendJSON({action: "getDictionary", sid: '1231'});
            });

            it('Should fail examine by badSid', function (done) {
                player.ws.onmessage = function (e) {
                    var data = JSON.parse(e.data);
                    if (data['action'] == 'examine') {
                        expect(data['result']).to.equal('badSid');
                        player.ws.onmessage = undefined;
                        done();
                    }
                }
                player.ws.sendJSON({action: "examine", sid: 'fdsf'});
            });

            it('Should fail examine by badId', function (done) {
                player.ws.onmessage = function (e) {
                    var data = JSON.parse(e.data);
                    if (data['action'] == 'examine') {
                        expect(data['result']).to.equal('badId');
                        player.ws.onmessage = undefined;
                        done();
                    }
                }
                player.ws.sendJSON({action: "examine", sid: player.sid, id: 10000000});
            });

            it('Should successfully make self examine', function (done) {
                player.ws.onmessage = function (e) {
                    var data = JSON.parse(e.data);
                    if (data['action'] == 'examine') {
                        expect(data['result']).to.equal('ok');
                        expect(data['id']).to.equal(player['id']);
                        expect(data['type']).to.equal('player');
                        expect(data['login']).to.equal(player['login']);
                        expect(data['x']).to.be.at.least(0);
                        expect(data['y']).to.be.at.least(0);
                        player.ws.onmessage = undefined;
                        done();
                    }
                }
                player.ws.sendJSON({action: "examine", sid: player.sid, id: player['id']});
            });

            it('Should fail look by badSid', function (done) {
                player.ws.onmessage = function (e) {
                    var data = JSON.parse(e.data);
                    if (data['action'] == 'look') {
                        expect(data['result']).to.equal('badSid');
                        player.ws.onmessage = undefined;
                        done();
                    }
                }
                player.ws.sendJSON({action: "look", sid: 'fds'});
            });

            it('Should successfully look and should contain positive player coordinates', function (done) {
                player.ws.onmessage = function (e) {
                    var data = JSON.parse(e.data);
                    if (data['action'] == 'look') {
                        expect(data['x']).to.be.at.least(0);
                        expect(data['y']).to.be.at.least(0);
                        player.ws.onmessage = undefined;
                        done();
                    }
                }
                player.ws.sendJSON({action: "look", sid: player.sid});
            });

            it('Should successfully look and should contain map and actors keys', function (done) {
                player.ws.onmessage = function (e) {
                    var data = JSON.parse(e.data);
                    if (data['action'] == 'look') {
                        expect(data).to.have.property('map');
                        expect(data).to.have.property('actors')
                        player.ws.onmessage = undefined;
                        done();
                    }
                }
                player.ws.sendJSON({action: "look", sid: player.sid });
            });

            it('Should successfully start testing', function (done) {
                player.ws.onmessage = function (e) {
                    var data = JSON.parse(e.data);
                    if (data['action'] == "startTesting") {
                        expect(data['result']).to.equal('ok');
                        player.ws.onmessage = undefined;
                        done();
                    }
                }
                player.ws.sendJSON({action: "startTesting", sid: player.sid});
            });

            it('Should successfully set up constants', function (done) {
                player.ws.onmessage = function (e) {
                    var data = JSON.parse(e.data);
                    if (data['action'] == "setUpConst") {
                        expect(data['result']).to.equal('ok');
                        player.ws.onmessage = undefined;
                        done();
                    }
                }
                player.ws.sendJSON(
                    { action: "setUpConst"
                    , sid: player.sid
                    , playerVelocity: pv
                    , slideThreshold: st
                    , ticksPerSecond: tps
                    , screenRowCount: rc
                    , screenColumnCount: cc
                    , pickUpRadius: pur // TODO add in server
                    }
                );
            });

            it('Should successfully get constants', function (done) {
                player.ws.onmessage = function (e) {
                    var data = JSON.parse(e.data);
                    if (data['action'] == "getConst") {
                        console.log(data['playerVelocity']);
                        console.log(pv);
                        console.log(data['slideThreshold']);
                        console.log(st);
                        expect(Math.abs(data['playerVelocity'] - pv)).to.be.below(0.01);
                        expect(Math.abs(data['slideThreshold'] - st)).to.be.below(0.01);
                        expect(data['ticksPerSecond']).to.equal(tps);
                        expect(data['screenRowCount']).to.equal(rc);
                        expect(data['screenColumnCount']).to.equal(cc);

                        player.ws.onmessage = undefined;
                        done();
                    }
                }
                player.ws.sendJSON({action: "getConst", sid: player.sid});
            });

            it('Should fail get constants', function (done) {
                player.ws.onmessage = function (e) {
                    var data = JSON.parse(e.data);
                    if (data['action'] == "getConst") {
                        expect(data['playerVelocity']).to.not.equal(1.0);
                        player.ws.onmessage = undefined;
                        done();
                    }
                }
                player.ws.sendJSON({action: "getConst", sid: player.sid});
            });

            it('Should fail set up empty map', function (done) {
                player.ws.onmessage = function (e) {
                    var data = JSON.parse(e.data);
                    console.log(data);
                    if (data['action'] == "setUpMap") {
                        expect(data['result']).to.equal('badMap');
                        player.ws.onmessage = undefined;
                        done();
                    }
                }
                player.ws.sendJSON({action: "setUpMap", sid: player.sid, map: [[]]});
            });

            it('Should successfully set up map', function (done) {
                utils.setMap(player, map, expect, done);
            });

            it('Should successfully end test', function (done) {
                player.ws.sendJSON({action: 'stopTesting', sid: player.sid});
                done();
            });

            it('Logout', function (done) {
                player.ws.sendJSON({action: "logout", sid: player.sid});
                done();
            });
        });

    }

    return {
        Test: Test
    }
});