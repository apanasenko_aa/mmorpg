/**
 * Created by Alexander on 5/28/14.
 */
define(function () {
    function WSConnect(wsUrl, onopen, onclose, onmessage) {
        var socket = null;
        if (!wsUrl)
            console.log("ws URL is empty");
        socket = new WebSocket(wsUrl);
        socket.onopen = onopen;
        socket.onclose = onclose;
        socket.onmessage = onmessage;
        socket.sendJSON = function (/*JSON*/ msg) {
            socket.send(JSON.stringify(msg));
            console.log(JSON.stringify(msg));
        };
        return socket;
    }

    return WSConnect;
});