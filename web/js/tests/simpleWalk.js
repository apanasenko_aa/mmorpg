/**
 * Created by Alexander on 5/31/14.
 */
define(['utils'], function (utils) {

    var EPS = 0.2 ;//TODO Add EPS to protocol
    var data = null;
    var expect = chai.expect;
    var speed = null;
    var map = [
        ["#", "#", "#", "#", "#", "#", "#"],
        ["#", ".", ".", "#", ".", ".", "#"],
        ["#", ".", ".", ".", ".", ".", "#"],
        ["#", ".", ".", "#", ".", ".", "#"],
        ["#", "#", "#", "#", "#", "#", "#"]
    ]

    function Test() {
        describe('Simple walk', function () {

            it('Register player 1', function (done) {
                this.timeout(10000);
                player = utils.createPlayer();
                player.speed = 0.15;
                utils.register(player, expect,
                    function () {utils.login(player, expect,
                        function () {utils.startTest(player, expect,
                            function() {utils.setMap(player, map, expect,
                                function() {utils.setSpeed(player,
                                    done
                                )}
                            )}
                        )}
                    )}
                );
            });

            it('Should successfully set player location', function (done) {
                this.timeout(10000);
                player.x =  2.0;
                player.y =  3.0;
                utils.setPosition(player, expect,
                    function() {utils.wait(player, 5,
                        function() {utils.checkPosition(player, player.x, player.y, 0.01, expect,
                            done
                        )}
                    )}
                );
            });

            it('Player should successfully move\n [all direction]', function (done) {
                this.timeout(10000);
                player.ws.onmessage = function (e) {
                    var data = JSON.parse(e.data);
                    if (data['action'] == "getConst") {
                        player.speed = data['playerVelocity'];
                        player.ws.onmessage = undefined;
                        console.log("speed")
                        console.log(speed);
                        console.log(data);
                        utils.makeStepsTo(player, "north", 1,
                            function(){utils.checkPosition(player, player.x, player.y - player.speed, 0.01, expect,

                                function(){utils.makeStepsTo(player, "south", 1,
                                    function(){utils.checkPosition(player, player.x, player.y, 0.01, expect,

                                        function(){utils.makeStepsTo(player, "west", 1,
                                            function(){utils.checkPosition(player, player.x - player.speed, player.y, 0.01, expect,

                                                function(){utils.makeStepsTo(player, "east", 1,
                                                    function(){utils.checkPosition(player, player.x, player.y, 0.01, expect,
                                                        done
                                                    )}
                                                )}
                                            )}
                                        )}
                                    )}
                                )}
                            )}
                        )
                    }
                }
                player.ws.sendJSON({action: "getConst", sid: player.sid});
            });

            it('Player should successfully abut the walls\n [north, wast]', function (done) {
                this.timeout(10000);
                player.x =  1.5;
                player.y =  1.5;
                utils.setPosition(player, expect,
                    function() {utils.wait(player, 5,

                        function(){utils.makeStepsTo(player, "north", 1,
                            function(){utils.checkPosition(player, player.x, player.y, 0.01, expect,

                                function(){utils.makeStepsTo(player, "west", 1,
                                    function(){utils.checkPosition(player, player.x, player.y, 0.01, expect, done)}
                                )}
                            )}
                        )}
                    )}
                );
            });

            it('Player should successfully abut the walls\n [south, east]', function (done) {
                this.timeout(10000);
                player.x =  2.5;
                player.y =  3.5;
                utils.setPosition(player, expect,
                    function() {utils.wait(player, 5,

                        function(){utils.makeStepsTo(player, "south", 1,
                            function(){utils.checkPosition(player, player.x, player.y, 0.01, expect,

                                function(){utils.makeStepsTo(player, "east", 1,
                                    function(){utils.checkPosition(player, player.x, player.y, 0.01, expect, done)}
                                )}
                            )}
                        )}
                    )}
                );
            });

            it('Player should successfully come to walls \n [speed = 0.4, south, east]', function (done) {
                this.timeout(10000);
                player.x =  1.5;
                player.y =  1.5;
                player.speed = 0.4;
                utils.setPosition(player, expect,
                    function() {utils.setSpeed(player,
                        function() {utils.wait(player, 5,

                            function() {utils.makeStepsTo(player, "south", 20,
                                function(){utils.checkPosition(player, 1.5, 3.5, EPS, expect,

                                    function() {utils.makeStepsTo(player, "east", 5,
                                        function(){utils.checkPosition(player, 2.5, 3.5, EPS, expect,
                                            done
                                        )}
                                    )}
                                )}
                            )}
                        )}
                    )}
                )
            });

            it('Player should successfully come to walls \n [speed = 0.4, north, west]', function (done) {
                this.timeout(10000);
                player.x =  2.5;
                player.y =  3.5;
                player.speed = 0.4;
                player.slideThreshold = 0.2
                utils.setPosition(player, expect,
                    function() {utils.setSpeed(player,
                        function() {utils.wait(player, 5,

                            function() {utils.makeStepsTo(player, "north", 20,
                                function(){utils.checkPosition(player, 2.5, 1.5, EPS, expect,

                                    function() {utils.makeStepsTo(player, "west", 5,
                                        function(){utils.checkPosition(player, 1.5, 1.5, EPS, expect,
                                            done
                                        )}
                                    )}
                                )}
                            )}
                        )}
                    )}
                )
            });

            it('Player should successfully come to walls \n [speed = 2.0, south, east]', function (done) {
                this.timeout(10000);
                player.x =  1.5;
                player.y =  1.5;
                player.speed = 2.0;
                utils.setPosition(player, expect,
                    function() {utils.setSpeed(player,
                        function() {utils.wait(player, 5,

                            function(){utils.makeStepsTo(player, "south", 4,
                                function(){utils.checkPosition(player, 1.5, 3.5, EPS, expect,

                                    function(){utils.makeStepsTo(player, "east", 2,
                                        function(){utils.checkPosition(player, 2.5, 3.5, EPS, expect,
                                            done
                                        )}
                                    )}
                                )}
                            )}
                        )}
                    )}
                )
            });

            it('Player should successfully come to walls \n [speed = 2.0, north, west]', function (done) {
                this.timeout(10000);
                player.x =  5.5;
                player.y =  3.5;
                player.speed = 2.0;
                utils.setPosition(player, expect,
                    function() {utils.setSpeed(player,
                        function() {utils.wait(player, 5,

                            function(){utils.makeStepsTo(player, "north", 4,
                                function(){utils.checkPosition(player, 5.5, 1.5, EPS, expect,

                                    function(){utils.makeStepsTo(player, "west", 2,
                                        function(){utils.checkPosition(player, 4.5, 1.5, EPS, expect,
                                            done
                                        )}
                                    )}
                                )}
                            )}
                        )}
                    )}
                )
            });

            it('Player should successfully abut the walls, slide threshold less than corner wall\n [east, wast]', function (done) {
                this.timeout(10000);
                player.speed = 0.5;
                player.slideThreshold = 0.1;
                player.x = 1.5;
                player.y = 1.5 - player.slideThreshold  - 0.001;
                map = [
                    ["#", ".", "#"],
                    [".", ".", "."],
                    ["#", ".", "#"]
                ];
                utils.setMap(player, map, expect,
                    function() {utils.setUpCons(player, expect,
                        function() {utils.setPosition(player, expect,
                            function() {utils.wait(player, 5,

                                function(){utils.makeStepsTo(player, "east", 3,
                                    function(){utils.checkPosition(player, player.x, player.y, 0.01, expect,

                                        function(){utils.makeStepsTo(player, "west", 2,
                                            function(){utils.checkPosition(player, player.x, player.y, 0.01, expect, done)}
                                        )}
                                    )}
                                )}
                            )}
                        )}
                    )}
                );
            });

            it('Player should successfully abut the walls, slide threshold less than corner wall\n [north, south]', function (done) {
                this.timeout(10000);
                player.speed = 0.5;
                player.slideThreshold = 0.1;
                player.x = 1.5 - player.slideThreshold  - 0.001;
                player.y = 1.5;
                map = [
                    ["#", ".", "#"],
                    [".", ".", "."],
                    ["#", ".", "#"]
                ];
                utils.setMap(player, map, expect,
                    function() {utils.setUpCons(player, expect,
                        function() {utils.setPosition(player, expect,
                            function() {utils.wait(player, 5,

                                function(){utils.makeStepsTo(player, "south", 3,
                                    function(){utils.checkPosition(player, player.x, player.y, 0.01, expect,

                                        function(){utils.makeStepsTo(player, "north", 2,
                                            function(){utils.checkPosition(player, player.x, player.y, 0.01, expect, done)}
                                        )}
                                    )}
                                )}
                            )}
                        )}
                    )}
                );
            });

            it('Player should successfully abut the walls, slide threshold less than corner wall\n [east, wast]', function (done) {
                this.timeout(10000);
                player.speed = 0.5;
                player.slideThreshold = 0.1;
                player.x = 1.5;
                player.y = 1.5 + player.slideThreshold  + 0.001;
                map = [
                    ["#", ".", "#"],
                    [".", ".", "."],
                    ["#", ".", "#"]
                ];
                utils.setMap(player, map, expect,
                    function() {utils.setUpCons(player, expect,
                        function() {utils.setPosition(player, expect,
                            function() {utils.wait(player, 5,

                                function(){utils.makeStepsTo(player, "east", 3,
                                    function(){utils.checkPosition(player, player.x, player.y, 0.01, expect,

                                        function(){utils.makeStepsTo(player, "west", 2,
                                            function(){utils.checkPosition(player, player.x, player.y, 0.01, expect, done)}
                                        )}
                                    )}
                                )}
                            )}
                        )}
                    )}
                );
            });

            it('Player should successfully abut the walls, slide threshold less than corner wall\n [north, south]', function (done) {
                this.timeout(400000);
                player.speed = 0.5;
                player.slideThreshold = 0.1;
                player.x = 1.5 + player.slideThreshold  + 0.001;
                player.y = 1.5;
                map = [
                    ["#", ".", "#"],
                    [".", ".", "."],
                    ["#", ".", "#"]
                ];
                utils.setMap(player, map, expect,
                    function() {utils.setUpCons(player, expect,
                        function() {utils.setPosition(player, expect,
                            function() {utils.wait(player, 5,

                                function(){utils.makeStepsTo(player, "south", 3,
                                    function(){utils.checkPosition(player, player.x, player.y, 0.01, expect,

                                        function(){utils.makeStepsTo(player, "north", 2,
                                            function(){utils.checkPosition(player, player.x, player.y, 0.01, expect, done)}
                                        )}
                                    )}
                                )}
                            )}
                        )}
                    )}
                );
            });

            it('Player should successfully move, slide threshold more than corner wall\n [All diraction]', function (done) {
                this.timeout(10000);
                player.speed = 0.0999;
                player.slideThreshold = 0.1;
                player.x = 1.5;
                player.y = 1.5 - player.slideThreshold  + 0.001;
                map = [
                    ["#", ".", "#"],
                    [".", ".", "."],
                    ["#", ".", "#"]
                ];
                utils.setMap(player, map, expect,
                    function() {utils.setUpCons(player, expect,
                        function() {utils.setPosition(player, expect,
                            function() {utils.wait(player, 5,

                                function(){utils.makeStepsTo(player, "east", 1,
                                    function(){utils.checkPosition(player, 1.5 + player.speed, 1.5, 0.001, expect,

                                        function(){utils.makeStepsTo(player, "south", 1,
                                            function(){utils.checkPosition(player, 1.5, 1.5 + player.speed, 0.001, expect,

                                                function(){utils.makeStepsTo(player, "west", 1,
                                                    function(){utils.checkPosition(player, 1.5 - player.speed, 1.5, 0.001, expect,

                                                        function(){utils.makeStepsTo(player, "north", 1,
                                                            function(){utils.checkPosition(player, 1.5, 1.5 - player.speed, 0.001, expect, done)}
                                                        )}
                                                    )}
                                                )}
                                            )}
                                        )}
                                    )}
                                )}
                            )}
                        )}
                    )}
                );
            });

            it('End test', function (done) {
                player.ws.sendJSON({action: 'stopTesting', sid: player.sid});
                player.ws.sendJSON({action: "logout", sid: player.sid})
                done();
            });
        });
    }

    return {
        Test: Test
    }
});