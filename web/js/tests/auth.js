define(['utils'], function (utils) {

    function Test() {
        var expect = chai.expect;
        var login = null;
        var password = null;
        var sid = null;
        var data = null;

        describe('Registration', function () {
            it('Should fail register by badLogin[empty]', function (done) {
                data = {
                    'login': '',
                    'password': utils.createPassword(),
                    'class': "mage",
                    'action': 'register'
                }
                utils.sendHttp(data, function (data) {
                    expect(data['result']).to.equal('badLogin');
                    done();
                });
            });

            it('Should fail register by badLogin[short]', function (done) {
                data = {
                    'login': 'a',
                    'password': utils.createPassword(),
                    'class': "mage",
                    'action': 'register'
                }
                utils.sendHttp(data, function (data) {
                    expect(data['result']).to.equal('badLogin');
                    done();
                });
            });

            it('Should fail register by badLogin[short]', function (done) {
                data = {
                    'login': '+e',
                    'password': utils.createPassword(),
                    'class': "mage",
                    'action': 'register'
                }
                utils.sendHttp(data, function (data) {
                    expect(data['result']).to.equal('badLogin');
                    done();
                });
            });

            it('Should fail register by badLogin[long]', function (done) {
                data = {
                    'login': 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',
                    'password': utils.createPassword(),
                    'action': 'register'
                }
                utils.sendHttp(data, function (data) {
                    expect(data['result']).to.equal('badLogin');
                    done();
                });
            });

            it('Should fail register by badLogin[unsupported chars]', function (done) {
                data = {
                    'login': '+-/:::-;-*&^',
                    'password': utils.createPassword(),
                    'class': "mage",
                    'action': 'register'
                }
                utils.sendHttp(data, function (data) {
                    expect(data['result']).to.equal('badLogin');
                    done();
                });
            });

            it('Should fail register by badLogin[long]', function (done) {
                data = {
                    'login': '1111111111111111111111111111111111111',
                    'password': utils.createPassword(),
                    'class': "mage",
                    'action': 'register'
                }
                utils.sendHttp(data, function (data) {
                    expect(data['result']).to.equal('badLogin');
                    done();
                });
            });

            it('Should fail register by badPassword[short]', function (done) {
                data = {
                    'login': utils.createLogin(),
                    'password': 'a',
                    'class': "mage",
                    'action': 'register'
                }
                utils.sendHttp(data, function (data) {
                    expect(data['result']).to.equal('badPassword');
                    done();
                });
            });

            it('Should fail register by badPassword[long]', function (done) {
                data = {
                    'login': utils.createLogin(),
                    'password': 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',
                    'class': "mage",
                    'action': 'register'
                }
                utils.sendHttp(data, function (data) {
                    expect(data['result']).to.equal('badPassword');
                    done();
                });
            });

            it('Should successfully register', function (done) {
                this.timeout(10000); // My compute very slow ((
                login = utils.createLogin();
                password = utils.createPassword();
                data = {
                    'login': login,
                    'password': password,
                    'class': "mage",
                    'action': 'register'
                }
                utils.sendHttp(data, function (data) {
                    expect(data['result']).to.equal('ok');
                    done();
                });
            });

            it('Should fail register by loginExists', function (done) {
                data = {
                    'login': login,
                    'password': password,
                    'class': "mage",
                    'action': 'register'
                }
                utils.sendHttp(data, function (data) {
                    expect(data['result']).to.equal('loginExists');
                    done();
                });
            });
        });


        describe('Login', function () {

            it('Should successfully login', function (done) {
                this.timeout(500);
                data = {
                    'login': login,
                    'password': password,
                    'class': "mage",
                    'action': 'login'
                }
                utils.sendHttp(data, function (data) {
                    expect(data['result']).to.equal('ok');
                    sid = data['sid'];
                    done();
                });
            });

            it('Should fail login by invalidCredentials[wrong login]', function (done) {
                data = {
                    'login': utils.createLogin(),
                    'password': password,
                    'class': "mage",
                    'action': 'login'
                }
                utils.sendHttp(data, function (data) {
                    expect(data['result']).to.equal('invalidCredentials');
                    done();
                });
            });

            it('Should fail login by invalidCredentials[wrong password]', function (done) {
                data = {
                    'login': login,
                    'password': utils.createPassword(),
                    'class': "mage",
                    'action': 'login'
                }
                utils.sendHttp(data, function (data) {
                    expect(data['result']).to.equal('invalidCredentials');
                    done();
                });
            });

            it('Should successfully logout', function (done) {
                data = {
                    'sid': sid,
                    'action': 'logout'
                }
                utils.sendHttp(data, function (data) {
                    expect(data['result']).to.equal('ok');
                    done();
                });
            });

            it('Should fail logout by badSid', function (done) {
                data = {
                    'sid': sid,
                    'action': 'logout'
                }
                utils.sendHttp(data, function (data) {
                    expect(data['result']).to.equal('badSid');
                    done();
                });
            });

            it('Should fail logout by badSid', function (done) {
                data = {
                    'sid': '',
                    'action': 'logout'
                }
                utils.sendHttp(data, function (data) {
                    expect(data['result']).to.equal('badSid');
                    done();
                });
            });

            it('Should successfully login', function (done) {
                this.timeout(1000);
                data = {
                    'login': login,
                    'password': password,
                    'class': "mage",
                    'action': 'login'
                }
                utils.sendHttp(data, function (data) {
                    expect(data['result']).to.equal('ok');
                    sid = data['sid'];
                    done();
                });
            });

            it('Should successfully logout', function (done) {
                data = {
                    'sid': sid,
                    'action': 'logout'
                }
                utils.sendHttp(data, function (data) {
                    expect(data['result']).to.equal('ok');
                    done();
                });
            });
        })
    }

    return {
        Test: Test
    }
});