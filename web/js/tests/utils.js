/**
 * Created by Alexander on 5/28/14.
 */
define(['ws'], function (wsConnect) {

    var httpUrl = null;

    function checkMob(curPlayer, mob, result, expect, done) {
        curPlayer.ws.onmessage = function (e) {
            var data = JSON.parse(e.data);
            if (data['action'] == 'examine') {
                expect(data['result']).to.equal(result);
                curPlayer.ws.onmessage = undefined;
                done();
            }
        }
        curPlayer.ws.sendJSON({action: "examine", sid: curPlayer.sid, id: mob.id});
    }

    function checkPosition(curPlayer, x, y, EPS, expect, done) {
        curPlayer.ws.onmessage = function (e) {
            var data = JSON.parse(e.data);
            if (data['action'] == "look") {
                console.log("check Position");
                console.log(x);
                console.log(y);
                console.log(data);

                curPlayer.ws.onmessage = undefined;
                console.log(data['x']);
                console.log(data['y']);
                expect(Math.abs(data['x'] - x)).to.be.below(EPS);
                expect(Math.abs(data['y'] - y)).to.be.below(EPS);
                done();
            }
        }
        curPlayer.ws.sendJSON({ action: "look", sid: curPlayer.sid });
    }

    function checkPositionWithExamine(curPlayer, mob, x, y, eps, expect, done) {
        curPlayer.ws.onmessage = function (e) {
            var data = JSON.parse(e.data);
            if (data['action'] == 'examine') {
                mob.x = data['x'];
                mob.y = data['y'];
                expect(data['result']).to.equal('ok');
                expect(data['id']).to.equal(mob.id);
                expect(Math.abs(data['x'] - x)).to.be.below(eps);
                expect(Math.abs(data['y'] - y)).to.be.below(eps);
                curPlayer.ws.onmessage = undefined;
                done();
            }
        }
        curPlayer.ws.sendJSON({action: "examine", sid: curPlayer.sid, id: mob.id});
    }

    function createLogin() {
        return 'test' + randString();
    }

    function createPassword() {
        return 'password' + randString();
    }

    function createPlayer(){
        return {
            'login': createLogin(),
            'password': createPassword(),
            'class': "mage"
        };
    }

    function examine(curPlayer, mob, expect, done){
        curPlayer.ws.onmessage = function (e) {
            var data = JSON.parse(e.data);
            if (data['action'] == 'examine') {
                console.log(data);
                expect(data['result']).to.equal('ok');
                mob.x = data['x'];
                mob.y = data['y'];
                done();
            }
        }
        curPlayer.ws.sendJSON({action: "examine", sid: curPlayer.sid, id: mob.id});
    }

    function fightWithMob(curPlayer, mob, countHit, done){
        var countTickToRefreshCoord = 0;
        var countTick = 0;
        curPlayer.ws.onmessage = function (e) {
            var data = JSON.parse(e.data);
            if (data['tick']) {
                countTick++;
                countTickToRefreshCoord++;
            }
            if (!countHit){
                curPlayer.ws.onmessage = undefined;
                done()
            }
            if (countTick > 2) {
                countHit--;
//                        curPlayer.ws.sendJSON(
//                            { action: "use"
//                            , id: 1 //  TODO send fist ID
//                            , sid: curPlayer.sid
//                            , x: mob.x
//                            , y: mob.y
//                            });
                curPlayer.ws.sendJSON(
                    { action: "use"
                        , sid: curPlayer.sid
                        , x: mob.x
                        , y: mob.y
                        , id: curPlayer.fistId
                    });
                countTick = 0;
            }
            if (countTickToRefreshCoord > 10){
                curPlayer.ws.sendJSON({action: "examine", sid: curPlayer.sid, id: mob.id});
                countTickToRefreshCoord = 0;
            }
            if (data['action'] == 'examine') {
                if (data['result'] == 'badId') {
                    curPlayer.ws.onmessage = undefined;
                    done();
                }
                mob.x = data['x'];
                mob.x = data['y'];
            }
        }
    }

    function login(curPlayer, expect, done){
        var data = {
            'login': curPlayer['login'],
            'password': curPlayer['password'],
            'class': curPlayer['class'],
            'action': 'login'
        };
        sendHttp(data, function (data) {
            console.log(data);
            expect(data['result']).to.equal('ok');
            curPlayer['id'] = data['id'];
            curPlayer['sid'] = data['sid'];
            curPlayer.fistId = data.fistId;
            curPlayer['ws'] = wsConnect(data['webSocket']);
            curPlayer['ws'].onopen = function () {
                console.log("wedSocket connect : ok");
                done();
            };

        });
    }

    function makeStepsTo(curPlayer, direction, count, done){
        var countTick = 0;
        curPlayer.ws.onmessage = function (e) {
            var data = JSON.parse(e.data);
            if (data['tick']) {
                countTick++;
            }
            if (!count && countTick > 4){
                curPlayer.ws.onmessage = undefined;
                done()
            }
            if (count && countTick > 2) {
                count--;
                curPlayer.ws.sendJSON(
                    { action: "move"
                        , direction: direction
                        , sid: curPlayer.sid
                    });
                countTick = 0;
            }
        }
    }

    function putMob(curPlayer, mob, expect, done){
        curPlayer.ws.onmessage = function (e) {
            var data = JSON.parse(e.data);
            if (data['action'] == "putMob") {
                console.log("Put mob");
                console.log(data);
                curPlayer.ws.onmessage = undefined;
                expect(data['result']).to.equal('ok');
                mob['id'] = data['id'];
                done();
            }
        }
        curPlayer.ws.sendJSON(
            { action: "putMob"
                , sid: curPlayer.sid
                , x: mob.x
                , y: mob.y
                , stats: mob.stats
                , flags: mob.flags
                , race: mob.race
                , dealtDamage: mob.damage
            });

    }

    function randString() {
        var text = '';
        var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (var i = 0; i < 10; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    }

    function register(curPlayer, expect, done){
        var data = {
                'login': curPlayer['login'],
                'password': curPlayer['password'],
                'class': curPlayer['class'],
                'action': 'register'
            };
        sendHttp(data, function (data) {
            console.log(data);
            expect(data['result']).to.equal('ok');
            done();
        });
    }

    function sendHttp(data, callback) {
        $.ajax({
            type: 'POST',
            url: httpUrl || "doaction.jsp",
            data: JSON.stringify(data),
            success: callback
        });
    }

    function setHttpUrl(url) {
        httpUrl = url;
    }

    function setMap(curPlayer, map, expect, done){
        curPlayer.ws.onmessage = function (e) {
            var data = JSON.parse(e.data);
            console.log(data);
            if (data['action'] == "setUpMap") {
                expect(data['result']).to.equal('ok');
                curPlayer.ws.onmessage = undefined;
                done();
            }
        }
        curPlayer.ws.sendJSON({action: "setUpMap", sid: curPlayer.sid, map: map});
    }

    function setPosition(curPlayer, expect, done) {
        curPlayer.ws.onmessage = function (e) {
            var data = JSON.parse(e.data);
            if (data['action'] == "setLocation") {
                expect(data['result']).to.equal('ok');
                curPlayer.ws.onmessage = undefined;
                done();
            }
        }
        curPlayer.ws.sendJSON(
            { action: "setLocation"
                , sid: curPlayer.sid
                , x: curPlayer.x
                , y: curPlayer.y
            });
    }

    function setSpeed(curPlayer, done){
        curPlayer.ws.onmessage = function (e) {
            var data = JSON.parse(e.data);
            if (data['action'] == "getConst") {
                curPlayer.ws.sendJSON(
                    { action: "setUpConst"
                        , sid: curPlayer.sid
                        , playerVelocity: curPlayer.speed
                        , slideThreshold: data['slideThreshold']
                        , ticksPerSecond: data['ticksPerSecond']
                        , screenRowCount: data['screenRowCount']
                        , screenColumnCount: data['screenColumnCount']
                        , pickUpRadius: data['pickUpRadius']
                    }
                );
                curPlayer.slideThreshold = data['slideThreshold']; // TODO Make function!
                curPlayer.ws.onmessage = undefined;
                done();
            }
        }
        curPlayer.ws.sendJSON({action: "getConst", sid: curPlayer.sid});
    }

    function setUpConst(curPlayer, expect, done){
        curPlayer.ws.onmessage = function (e) {
            var data = JSON.parse(e.data);
            if (data['action'] == "setUpConst") {
                expect(data['result']).to.equal('ok');
                curPlayer.ws.onmessage = undefined;
                done();
            }
        }
        curPlayer.ws.sendJSON(
            { action: "setUpConst"
                , sid: curPlayer.sid
                , playerVelocity: curPlayer.speed
                , slideThreshold: curPlayer.slideThreshold
                , ticksPerSecond: 10
                , screenRowCount: 12
                , screenColumnCount: 6
                , pickUpRadius: 0.4 // TODO add in server
            }
        );
    }

    function startTest(curPlayer, expect, done){
        curPlayer.ws.onmessage = function (e) {
            var data = JSON.parse(e.data);
            console.log(data);
            if (data['action'] == "startTesting") {
                expect(data['result']).to.equal('ok');
                curPlayer.ws.onmessage = undefined;
                done();
            }
        }
        curPlayer.ws.sendJSON({action: "startTesting", sid: curPlayer.sid});
    }

    function wait(curPlayer, maxCount, done){
        var countTick = 0;
        curPlayer.ws.onmessage = function (e) {
            var data = JSON.parse(e.data);
            if (data['event']){
                console.log(data);
            }
            if (data['tick']) {
                countTick++;
            }
            if (countTick > maxCount) {
                curPlayer.ws.onmessage = undefined;
                countTick = 0;
                done();
            }
        }
    }

    return {

        checkMob: checkMob,
        checkPosition: checkPosition,
        checkPositionWithExamine: checkPositionWithExamine,
        createLogin: createLogin,
        createPassword: createPassword,
        createPlayer: createPlayer,
        examine: examine,
        fightWithMob: fightWithMob,
        login: login,
        makeStepsTo: makeStepsTo,
        putMob: putMob,
        randString: randString,
        register: register,
        sendHttp: sendHttp,
        setHttpUrl: setHttpUrl,
        setMap: setMap,
        setPosition: setPosition,
        setSpeed: setSpeed,
        setUpCons:  setUpConst,
        startTest: startTest,
        wait: wait
    }
})


