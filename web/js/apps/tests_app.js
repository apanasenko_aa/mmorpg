requirejs.config({
    baseUrl: 'js/tests',
    paths: {
        jquery: '../lib/jquery'
        , auth: 'auth'
        , prepare: 'prepare'
        , simpleWalk: 'simpleWalk'
        , mods: 'mobs'
    }
});

requirejs(
    ['jquery', 'utils', 'auth', 'prepare', 'simpleWalk', 'mobs'],
    function ($, utils, auth, prepare, sWalk, mobs) {
        $(function () {
            mocha.setup('bdd');
            var StartTesting = function () {
                $('#mocha').empty();
                utils.setHttpUrl($("#url").val());
//                auth.Test();
                prepare.Test();
                sWalk.Test();
                mobs.Test()
                mocha.run();
            };

            $("#urlBtn").click(StartTesting);
            $("#url").val("doaction.jsp").keydown(function (e) {
                if (e.which == 13) {
                    e.preventDefault();
                    StartTesting();
                }
            })
        });
    });