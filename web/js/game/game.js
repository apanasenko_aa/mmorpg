function Game() {
    this.view = null;
    this.socket = null;
    this.sid = '';
    this.socketUrl = '';
    this.playerId = '';
    this.tick = -1;
    this.prevTick = -1;
    this.keysDown = {};
}

Game.prototype.initGame = function () {
    this.sid = getUrlVars()['sid'];
    this.socketUrl = getUrlVars()['websocket'];
    this.playerId = getUrlVars()['id'];
    this.fistId = getUrlVars()['fistId'];
    this.view = new View();
    if ('WebSocket' in window) {
        this.socket = new WebSocket(this.socketUrl);
    } else {
        alert('Error: WebSocket is not supported by this browser.');
        exitGame();
        return;
    }

    this.socket.onopen = function () {
        game.getDictionary();
        game.getConst();
    };

    this.socket.onclose = function () {
        game.view.setActors({});
        game.view.setMap({});
        game.view.updateView(game.playerId);
        exitGame();
    };

    this.socket.onmessage = function (message) {
        game.receiveMsg(JSON.parse(message.data));
    };

    requestAnimFrame(animate);
};

Game.prototype.sendMsg = function (msg) {
    this.socket.send(JSON.stringify(msg));
};

Game.prototype.getDictionary = function () {
    this.sendMsg({
        action: "getDictionary",
        sid: this.sid
    });
};

Game.prototype.getConst = function () {
    this.sendMsg({
        action: "getConst"
      , sid: this.sid
    });
};

Game.prototype.look = function () {
    this.sendMsg({
        action: "look",
        sid: this.sid
    });
};

Game.prototype.examine = function (id) {
    this.sendMsg({
        action: "examine",
        id: id,
        sid: this.sid
    });
};

Game.prototype.attack = function (x, y) {
    this.sendMsg({
        action: "use",
        x: x,
        y: y,
        id: parseInt(this.fistId),
        sid: this.sid
    });
};

Game.prototype.useSkill = function (x, y) {
    this.sendMsg({
        action: "useSkill",
        x: x,
        y: y,
        sid: this.sid
    });
};

Game.prototype.pickUp = function (itemID) {
    this.sendMsg({
        action: "pickUp",
        id: itemID,
        sid: this.sid
    });
};

Game.prototype.dropItem = function (itemID) {
    this.sendMsg({
        action: "drop",
        id: itemID,
        sid: this.sid
    });
};

Game.prototype.move = function (direction) {
    this.sendMsg({
        action: "move",
        direction: direction,
        tick: this.tick,
        sid: this.sid
    });
};

Game.prototype.logOut = function () {
    this.sendMsg({
        action: "logout",
        sid: this.sid
    });
};

function toFixed(value, precision) {
    var power = Math.pow(10, precision || 0);
    return String(Math.round(value * power) / power);
}

Game.prototype.receiveMsg = function (msg) {
    if (msg.hasOwnProperty('tick')) {
        this.tick = msg.tick;
        this.look();
        return;
    }
    if (!msg.hasOwnProperty('result')) return;
    if (msg.result == 'badSid') {
        alert('Invalid sid!');
        exitGame();
        return;
    }
    switch (msg.action) {
        case 'getConst':
            SIGHT_RADIUS_X = msg.screenColumnCount || 11;
            SIGHT_RADIUS_Y = msg.screenRowCount || 11;
            break;
        case 'getDictionary':
            this.view.setDictionary(msg.dictionary);
            break;
        case 'look':
            this.view.setMap(msg.map);
            this.view.setActors(msg.actors);
            this.view.setPlayerLocation(msg.x, msg.y);
            document.getElementById('exp_progress').max = msg.max_exp;
            document.getElementById('exp_progress').value = msg.cur_exp;
            break;
        case 'examine':

            document.getElementById('player_id').innerHTML = msg.id;
            document.getElementById('player_type').innerHTML = msg.type;
            document.getElementById('player_x').innerHTML = toFixed(msg.x, 4);
            document.getElementById('player_y').innerHTML = toFixed(msg.y, 4);
            if (msg.type == 'monster' || msg.type == 'player') {
                if (msg.type == 'monster') {
                    document.getElementById('player_race').innerHTML = msg.race || msg.mobType;
                    document.getElementById('player_name').innerHTML = msg.name;
                } else {
                    document.getElementById('player_name').innerHTML = msg.login;
                }
                this.view.setInventory(msg.inventory);
                this.view.setUpdateInventory();

                document.getElementById('player_hp').innerHTML = toFixed(msg.stats.hp, 2);
                document.getElementById('player_max_hp').innerHTML = toFixed(msg.stats.maxHp, 2);
                document.getElementById('hp_progress').max = msg.stats.maxHp;
                document.getElementById('hp_progress').value = msg.stats.hp;

                document.getElementById('player_mana').innerHTML = toFixed(msg.stats.mana, 2);
                document.getElementById('player_max_mana').innerHTML = toFixed(msg.stats.maxMana, 2);
                document.getElementById('mana_progress').max = msg.stats.maxMana;
                document.getElementById('mana_progress').value = msg.stats.mana;

                document.getElementById('player_speed').innerHTML = toFixed(msg.stats.speed, 4);
                document.getElementById('player_strength').innerHTML = msg.stats.strength;
                document.getElementById('player_agility').innerHTML = msg.stats.agility;
                document.getElementById('player_intelligence').innerHTML = msg.stats.intelligence;
            }

            if (msg.type == 'item') {
                document.getElementById('player_name').innerHTML = msg.name;
                document.getElementById('player_race').innerHTML = msg.itemType;
            }

            if (msg.examineType == 'item') {
                document.getElementById('examine').innerHTML =
                    '<div>ID: <span class="value">' + msg.id + '</span></div>' +
                    '<div>Имя: <span class="value">' + msg.name + '</span></div>' +
                    '<div>Тип: <span class="value">' + msg.type + '</span></div>' +
                    '<div>Описание: <span class="value">' + msg.description + '</span></div>';
            }

            break;
    }
};

Game.prototype.checkKeyboard = function () {
    if (this.keysDown[65]) this.move('west');
    if (this.keysDown[87]) this.move('north');
    if (this.keysDown[68]) this.move('east');
    if (this.keysDown[83]) this.move('south');
    if (this.keysDown[73]) this.examine(parseInt(this.playerId));
};

game = new Game();

function animate() {
    game.checkKeyboard();

    if (game.tick != game.prevTick) {
        game.prevTick = game.tick;
        game.view.updateView();
    }

    requestAnimFrame(animate);
}

document.onkeydown = function (e) {
    e = e || event;
    var code = e.keyCode;
    if (code == 81) {
        game.logOut();
    }
    if (e.shiftKey){
        game.view.setAttackTypes(2);
    }
    game.keysDown[code] = true;
};

document.onkeyup = function (e) {
    e = e || event;
    var code = e.keyCode;
    game.keysDown[code] = false;
    if (!e.shiftKey){
        game.view.setAttackTypes(0);
    }
};

game.initGame();