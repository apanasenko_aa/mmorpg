package MonsterQuest;

public enum PlayerClassType {
   FIGHTER, ROGUE, WIZARD;

   public static PlayerClassType strToPlayerClassType(String playerClassType) {
      switch (playerClassType) {
         case "warrior":
            return PlayerClassType.FIGHTER;
         case "rogue":
            return PlayerClassType.ROGUE;
         case "mage":
            return PlayerClassType.WIZARD;
         default:
            return PlayerClassType.FIGHTER;
      }
   }

   public static String toString(PlayerClassType playerClassType) {
      switch (playerClassType) {
         case FIGHTER:
            return "warrior";
         case ROGUE:
            return "rogue";
         case WIZARD:
            return "mage";
      }
      return "";
   }
}
