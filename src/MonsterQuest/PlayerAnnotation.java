package MonsterQuest;

import java.io.EOFException;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import com.sun.org.apache.bcel.internal.generic.NEW;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint(value = "/game") // TODO Вынести весь JSON в отдельный файл
// TODO  думал об этом, но туда надо будет передавать player'а и мб что то еще
public class PlayerAnnotation {

   private Player player;
   private Session openedSession;
   private static boolean isTesting = false;
   private static boolean isSetMap = true;
   private static boolean isEnforce = false;

   public static boolean isTest(){
      return isTesting;
   }

   public static JSONObject parseJsonString(String str) {
      JSONObject jsonResult = null;
      try {
         JSONParser jsonParser = new JSONParser();
         jsonResult = (JSONObject) jsonParser.parse(str);
      } catch (ParseException e) {
      }
      return jsonResult;
   }

   public JSONObject getBadAction(String action) {
      JSONObject result = new JSONObject();
      result.put("action", action);
      result.put("result", "badAction");
      return result;
   }

   public JSONObject getPositiveAnswer(String action) {
      JSONObject result = new JSONObject();
      result.put("action", action);
      result.put("result", "ok");
      return result;
   }

   public JSONObject getBadId(String action) {
      JSONObject result = new JSONObject();
      result.put("action", action);
      result.put("result", "badId");
      return result;
   }

   public JSONObject getBadSid(String action) {
      JSONObject result = new JSONObject();
      result.put("action", action);
      result.put("result", "badSid");
      return result;
   }

   public JSONObject getConst() {
      JSONObject result = getPositiveAnswer("getConst");
      result.put("playerVelocity", player.getSpeed());
      result.put("slideThreshold", Location.eps);
      result.put("ticksPerSecond", Game.ticksPerSecond);
      result.put("screenRowCount", GameMap.getSightRadiusY());
      result.put("screenColumnCount", GameMap.getSightRadiusX());
      result.put("pickUpRadius", Game.getPickUpRadius());
      return result;
   }

   public JSONObject getError() {
      JSONObject result = new JSONObject();
      result.put("result", "badAction");
      return result;
   }

   public JSONObject getLook() {
      JSONObject result = getPositiveAnswer("look");
      result.put("map", getMap((int) player.getLocation().x, (int) player.getLocation().y));
      result.put("actors", Game.getActorsList(player.getLocation()));
      result.put("x", player.getLocation().x);
      result.put("y", player.getLocation().y);
      result.put("cur_exp", player.getLevel().calcExpLevel());
      result.put("max_exp", player.getLevel().calcExpNextLevel());
      return result;
   }

   public JSONArray getMap(int x, int y) {
      return GameMap.mapToJson(x, y);
   }

   @OnOpen
   public void onOpen(Session session) {
      openedSession = session;
      Game.startTimer();
      Game.addSession(openedSession);
   }

   @OnMessage
   public void onMessage(String message) {
      JSONObject jsonMsg = parseJsonString(message);
      JSONObject jsonAns = new JSONObject();
      System.out.println(jsonMsg);
      String action = (String) jsonMsg.get("action");
      Player enforce_player = null;
      if (action.equals("enforce")){
         enforce_player = Game.findPlayerBySid((String) jsonMsg.get("sid"));
         isEnforce = true;
         jsonMsg = parseJsonString(jsonMsg.get("enforcedAction").toString());
         action = (String) jsonMsg.get("action");
      }
      System.out.println(action);

      String sid;
      try {
         sid = (String) jsonMsg.get("sid");
      } catch (Throwable e){
         sid = "";
      }

      player = Game.findPlayerBySid(sid); //TODO перенести логику в Game
      if (player == null) {
         player = UserDB.getPlayerBySid(sid, openedSession);
         if (player == null) {
            jsonAns = getBadSid(action);
            try {
               openedSession.getBasicRemote().sendText(jsonAns.toJSONString());
            } catch (Throwable e) {
            }
            return;
         }
         Game.addPlayer(player);
      }

      switch (action) {
         case "startTesting": {
            if (isTesting == true){
               jsonAns = getBadAction(action);
               jsonAns.put("action", action);
               break;
            }
            isTesting = true;
            isSetMap = false;
            jsonAns = getPositiveAnswer(action);
            break;
         }

         case "stopTesting": {
            if (!isTesting) {
               jsonAns = getBadAction(action);
               break;
            }
            isTesting = false;
            isSetMap = true;
            jsonAns = getPositiveAnswer(action);
            break;
         }

         case "setLocation": {
            if (!isTesting) {
               jsonAns = getBadAction(action);
               break;
            }
            try {
               Game.unsetMonsterInLocation(player.location);
               player.setLocation(
                     Double.parseDouble(jsonMsg.get("x").toString()),
                     Double.parseDouble(jsonMsg.get("y").toString())
               );
               Game.setMonsterInLocation(player);
               jsonAns = getPositiveAnswer(action);
            } catch (Throwable e) {
            }
            break;
         }

         case "putPlayer": {
            if (!isTesting) {
               jsonAns = getError();
               break;
            }
            try {
               Location location = null;
               try {
                  location = new Location(Double.parseDouble(jsonMsg.get("x").toString())
                        , Double.parseDouble(jsonMsg.get("y").toString()));
               } catch (Throwable e){
                  jsonAns = new JSONObject();
                  jsonAns.put("action", action);
                  jsonAns.put("result", "badPlacing");
                  break;
               }
               if (!isSetMap || !location.isLocationValid()){
                  jsonAns = new JSONObject();
                  jsonAns.put("action", action);
                  jsonAns.put("result", "badPlacing");
                  break;
               }
               Player new_player = new Player
                     ( Game.getNextGlobalId()
                           , Double.parseDouble(jsonMsg.get("x").toString())
                           , Double.parseDouble(jsonMsg.get("y").toString())
                           , player.getSession()
                     );
               if (jsonMsg.containsKey("stats")) {
                  JSONObject stats = (JSONObject) jsonMsg.get("stats");
                  new_player.stat.setStat(
                        stats.containsKey("strength") ? Integer.parseInt(stats.get("strength").toString()) : 0,
                        stats.containsKey("intelligence") ? Integer.parseInt(stats.get("intelligence").toString()) : 0,
                        stats.containsKey("agility") ? Integer.parseInt(stats.get("agility").toString()) : 0,
                        stats.containsKey("speed") ? Double.parseDouble(stats.get("speed").toString()) : Game.defaultSpeed,
                        stats.containsKey("defense") ? Integer.parseInt(stats.get("defense").toString()) : 10,
                        stats.containsKey("magic_resistance") ? Integer.parseInt(stats.get("magic_resistance").toString()) : 10,
                        stats.containsKey("capacity") ? Integer.parseInt(stats.get("capacity").toString()) : 10,
                        stats.containsKey("MAX_HP") ? Double.parseDouble(stats.get("MAX_HP").toString()) : 1000,
                        stats.containsKey("MAX_MP") ? Double.parseDouble(stats.get("MAX_MP").toString()) : 100,
                        stats.containsKey("HP") ? Double.parseDouble(stats.get("HP").toString()) : 1000,
                        stats.containsKey("MP") ? Double.parseDouble(stats.get("MP").toString()) : 100
                  );
               }
               if (jsonMsg.containsKey("inventory") && ((JSONArray)jsonMsg.get("inventory")).size() > 0) {
                  new_player.inventory.addItem(new Item("testItem", new Location(0, 0)));
               }
               Game.addPlayer(new_player);
               jsonAns = getPositiveAnswer(action);
               jsonAns.put("fistId", Monster.defoltEquipId);
               jsonAns.put("id", new_player.getId());
               jsonAns.put("inventory", new_player.getInventory().inventoryToJSON());
               jsonAns.put("sid", new_player.getSid());
            } catch (Throwable e) {
            }
            break;
         }

         case "putItem": {
            if (!isTesting) {
               jsonAns = getError();
               break;
            }
            try {
               Location location = null;
               try {
                  location = new Location(Double.parseDouble(jsonMsg.get("x").toString())
                        , Double.parseDouble(jsonMsg.get("y").toString()));
               } catch (Throwable e){
                  jsonAns = new JSONObject();
                  jsonAns.put("action", action);
                  jsonAns.put("result", "badPlacing");
                  break;
               }
               if (!isSetMap || !location.isLocationInGrass()){
                  jsonAns = new JSONObject();
                  jsonAns.put("action", action);
                  jsonAns.put("result", "badPlacing");
                  break;
               }
               Item item = new Item
                     (
                           "item_fo_test",
                           new Location(
                                 Double.parseDouble(jsonMsg.get("x").toString()),
                                 Double.parseDouble(jsonMsg.get("y").toString())
                           )
                     );
               Game.addItem(item);
               jsonAns = getPositiveAnswer(action);
               jsonAns.put("id", item.getId());
            } catch (Throwable e) {
            }
            break;
         }


         case "putMob": {
            if (!isTesting) {
               jsonAns = getError();
               break;
            }
            try {
               Location location = null;
               try {
                  location = new Location(Double.parseDouble(jsonMsg.get("x").toString())
                        , Double.parseDouble(jsonMsg.get("y").toString()));
               } catch (Throwable e){
                  jsonAns = new JSONObject();
                  jsonAns.put("action", action);
                  jsonAns.put("result", "badPlacing");
                  break;
               }
               if (!isSetMap || !location.isLocationValid()){
                  jsonAns = new JSONObject();
                  jsonAns.put("action", action);
                  jsonAns.put("result", "badPlacing");
                  break;
               }
               Race race = jsonMsg.containsKey("race")
                     ? Race.strToRace.get(jsonMsg.get("race").toString())
                     : Race.ANIMAL;
               if (race == null){
                  jsonAns = new JSONObject();
                  jsonAns.put("result", "badRace");
                  jsonAns.put("action", action);
                  break;
               }
               JSONArray flags = (JSONArray) jsonMsg.get("flags");
               boolean canMove = flags.remove("CAN_MOVE");
               boolean canBlow = flags.remove("CAN_BLOW");
               Set<Race> hateThem = new HashSet<>();
               boolean badFlag = false;
               for (Object flag : flags) {
                  badFlag = !Race.hateIs.containsKey(flag);
                  if (badFlag) {
                     jsonAns = new JSONObject();
                     jsonAns.put("action", action);
                     jsonAns.put("result", "badFlag");
                     break;
                  }
                  hateThem.add(Race.hateIs.get((String) flag));
               }
               hateThem.add(Race.DRAGON);
               if (badFlag) break;
               Blow blow = null;
               try {
                  String d = jsonMsg.containsKey("dealtDamage") ? jsonMsg.get("dealtDamage").toString() : "1d1";
                  String[] dealtDamage = d.split("d");
                  blow = new Blow(Integer.parseInt(dealtDamage[0]), Integer.parseInt(dealtDamage[1]));
               } catch (Throwable e) {
                  jsonAns.put("action", action);
                  jsonAns.put("result", "badDamage");
                  break;
               }
               Monster monster = Game.createStandartMonster
                     ("Anton"
                     , race
                     , canMove
                     , canBlow
                     , blow
                     , Double.parseDouble(jsonMsg.get("x").toString())
                     , Double.parseDouble(jsonMsg.get("y").toString())
                     );
               monster.hateThem = hateThem;
               if (jsonMsg.containsKey("stats")) {
                  JSONObject stats = (JSONObject) jsonMsg.get("stats");
                  monster.stat.setStat(
                        stats.containsKey("strength") ? Integer.parseInt(stats.get("strength").toString()) : 0,
                        stats.containsKey("intelligence") ? Integer.parseInt(stats.get("intelligence").toString()) : 0,
                        stats.containsKey("agility") ? Integer.parseInt(stats.get("agility").toString()) : 0,
                        stats.containsKey("speed") ? Double.parseDouble(stats.get("speed").toString()) : Game.defaultSpeed,
                        stats.containsKey("defense") ? Integer.parseInt(stats.get("defense").toString()) : 10,
                        stats.containsKey("magic_resistance") ? Integer.parseInt(stats.get("magic_resistance").toString()) : 10,
                        stats.containsKey("capacity") ? Integer.parseInt(stats.get("capacity").toString()) : 10,
                        stats.containsKey("MAX_HP") ? Double.parseDouble(stats.get("MAX_HP").toString()) : 1000,
                        stats.containsKey("MAX_MP") ? Double.parseDouble(stats.get("MAX_MP").toString()) : 100,
                        stats.containsKey("HP") ? Double.parseDouble(stats.get("HP").toString()) : 1000,
                        stats.containsKey("MP") ? Double.parseDouble(stats.get("MP").toString()) : 100
                  );
               }
               Game.addMonster(monster);
               jsonAns = getPositiveAnswer(action);
               jsonAns.put("id", monster.getId());
            } catch (Throwable e) {
               jsonAns = getBadAction(action);
            }
            break;
         }

         case "setUpConst": {
            if (!isTesting) {
               jsonAns = getError();
               break;
            }
            try{
               Location.eps = Double.parseDouble(jsonMsg.get("slideThreshold").toString());
               player.getStat().speed = Double.parseDouble(jsonMsg.get("playerVelocity").toString());
               Game.defaultSpeed = Double.parseDouble(jsonMsg.get("playerVelocity").toString());
               GameMap.setSightRadiusX(Integer.parseInt(jsonMsg.get("screenColumnCount").toString()));
               GameMap.setSightRadiusY(Integer.parseInt(jsonMsg.get("screenRowCount").toString()));
               Game.ticksPerSecond = Double.parseDouble(jsonMsg.get("ticksPerSecond").toString());
               Game.setPickUpRadius(Double.parseDouble(jsonMsg.get("pickUpRadius").toString()));
               jsonAns = getPositiveAnswer(action);
            } catch (Throwable e){
               jsonAns = getBadAction(action);
            }
            break;
         }

         case "setUpMap": {
            if (!isTesting) {
               jsonAns = getBadAction(action);
               break;
            }
            try {
               Game.resetGame((JSONArray) jsonMsg.get("map"), player);
               jsonAns = getPositiveAnswer(action);
               isSetMap = true;
            } catch (Throwable e){
               jsonAns = new JSONObject();
               jsonAns.put("action", action);
               jsonAns.put("result", "badMap");
            }
            break;
         }

         case "getMap": {
            if (!isTesting) {
               jsonAns = getError();
               break;
            }
            jsonAns = getPositiveAnswer(action);
            jsonAns.put("map", GameMap.getAllMap());
            break;
         }


         case "getConst": {
            jsonAns = getConst();
            break;
         }

         case "getDictionary": {
            jsonAns = getPositiveAnswer(action);
            jsonAns.put("dictionary", GameDictionary.getJsonDictionary());
            break;
         }

         case "examine": {
            try {
               Player examPlayer = Game.examinePlayer((long) jsonMsg.get("id"));
               Monster examMonster = Game.examineMonster((long) jsonMsg.get("id"));
               Item examItem = Game.examineItem((long) jsonMsg.get("id"));
               if (examItem == null) {
                  examItem = player.getInventory().getItem((long) jsonMsg.get("id"));
               }
               if (examPlayer != null) {
                  jsonAns = examPlayer.examine();

               } else if (examMonster != null) {
                  jsonAns = examMonster.examine();
               } else if (examItem != null) {
                  jsonAns = examItem.examine();
               } else {
                  jsonAns = getBadId(action);
               }

               break;
            } catch (Throwable e){
               jsonAns = getBadId(action);
               break;
            }
         }

         case "pickUp": {
            Long id = Long.parseLong(jsonMsg.get("id").toString());
            Item item = Game.getItem(id);
            if (item != null && Location.distance(item.getLocation(), player.getLocation()) < Game.getPickUpRadius()) {
               player.setInventoryId(id);
//               player.pickUpInventory();    --------- This is for tests, because other servers pick up
//               player.setInventoryId(-1);   --------- after request, but doesn't wait for tick
               jsonAns = getPositiveAnswer(action);
               break;
            } else {
               jsonAns = getBadId(action);
               break;
            }
         }

         case "drop": {
            if (player.getInventory().getItem((long) jsonMsg.get("id")) != null) {
               player.dropItem((long) jsonMsg.get("id"));
               jsonAns = getPositiveAnswer(action);
               break;
            } else {
               jsonAns = getBadId(action);
               break;
            }
         }

         case "destroyItem": {
            Long id = Long.parseLong(jsonMsg.get("id").toString());
            if (player.getInventory().getItem(id) != null) {
               player.destroyItem((long) jsonMsg.get("id"));
               jsonAns = getPositiveAnswer(action);
               break;
            } else if (Game.getItem(id) != null && Location.distance(Game.getItem(id).getLocation(), player.getLocation()) < Game.getPickUpRadius()) {
               Game.removeItems(Game.getItem(id));
            }else{
               jsonAns = getBadId(action);
               break;
            }
         }

         case "use": {
            if (jsonMsg.containsKey("id") && Integer.parseInt(jsonMsg.get("id").toString()) == Monster.defoltEquipId){
               player.playerAttack.setAttackId(1);
               player.setAim(
                     Double.parseDouble(jsonMsg.get("x").toString()),
                     Double.parseDouble(jsonMsg.get("y").toString()));
            }
            jsonAns = getPositiveAnswer(action);
            break;
         }

         case "useSkill": {
            player.playerAttack.setAttackId(2);
            player.setAim(
                  Double.parseDouble(jsonMsg.get("x").toString()),
                  Double.parseDouble(jsonMsg.get("y").toString()));
            return;
         }

         case "attack": {
            player.setAim((Double) jsonMsg.get("x"), (Double) jsonMsg.get("y"));
            return;
         }

         case "look": {
            jsonAns = getLook();
            break;
         }

         case "move": {
            player.setDirection(Direction.strToDirection((String) jsonMsg.get("direction")));
            jsonAns = getPositiveAnswer("move");
            break;
         }

         case "logout": {
            if (!player.logout()) {
               jsonAns = getBadSid(action);
            } else
               try {
                  jsonAns = getPositiveAnswer(action);
                  player.saveStateToBD();
                  player.getSession().close();
                  Game.unsetMonsterInLocation(player.getLocation());
                  Game.removePlayer(player);
               } catch (Throwable e) {
               }
            break;
         }

         default: {
            jsonAns = getBadAction(action);
            break;
         }
      }
      if (isEnforce){
         player = enforce_player;
         JSONObject ans = new JSONObject(jsonAns);
         jsonAns.put("actionResult", ans);
         jsonAns.put("action", "enforce");
         jsonAns.put("result", "ok");
         isEnforce = false;
      }
      System.out.println(jsonAns);
      player.sendMessage(jsonAns);
   }

   @OnClose
   public void onClose() {
      Game.removePlayer(player);
      Game.removeSession(openedSession);
   }


   @OnError
   public void onError(Throwable t) throws Throwable {
      // Most likely cause is a user closing their browser. Check to see if
      // the root cause is EOF and if it is ignore it.
      // Protect against infinite loops.
      int count = 0;
      Throwable root = t;
      while (root.getCause() != null && count < 20) {
         root = root.getCause();
         count++;
      }
      if (root instanceof EOFException) {
         // Assume this is triggered by the user closing their browser and
         // ignore it.
      } else {
         throw t;
      }
   }
}