package MonsterQuest;

import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.Set;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * Created by Alexander on 3/18/14.
 */
public class Monster extends ActiveObj {
   protected final Race type;
   protected final Inventory inventory = new Inventory();
   private long inventoryId = -1;
   protected Direction direction = Dice.getDirection();
   protected Monster aim;
   protected PlayerAttack playerAttack = new PlayerAttack();
   protected int timeToRefresh;

   protected final Level level = new Level();

   protected final Stat stat = new Stat();

   protected double aimX;
   protected double aimY;

   protected double currentAttackDelay;

   protected int expKill;
   protected int alertness;
   protected final ArrayList<Blow> blows;
   protected final ArrayList<Flag> flags;

   protected Bonus bonus;
   protected Set<Race> hateThem;

   protected boolean canMove = true;
   protected boolean canBlow = true;

   protected boolean clever = true;
   protected ArrayList<Location> way = null;
   protected int lenWay = 0;

   public static int defoltEquipId = 1;

   public Monster(
           long id,
           String name,
           Race type,
           double maxHp,
           double maxMana,
           double regenHp,
           double regenMana,
           double attackDelay,
           int expKill,
           int alertness,
           double speed,
           ArrayList<Blow> blows,
           ArrayList<Flag> flags,
           Location location,
           boolean generateInventory
   ) {
      super(id, name, location);
      this.type = type;
      this.stat.hp = maxHp;
      this.stat.mana = maxMana;
      this.stat.maxHp = maxHp;
      this.stat.maxMana = maxMana;
      this.stat.regenHp = regenHp;
      this.stat.regenMana = regenMana;
      this.stat.attackDelay = attackDelay;
      this.stat.speed = speed;
      this.expKill = expKill;
      this.blows = blows;
      this.flags = flags;
      this.alertness = alertness;
      this.aim = null;
      if (generateInventory) {
         generateRandomInventory();
      }
      this.bonus = inventory.calcBonus();
      this.playerAttack.setAttackId(1);
   }

   public void generateRandomInventory() {
      //TODO сколько раз бросать?
      for (int i = 0; i < Dice.getInt(1, 1); i++) {
         int itemTypeIndex;
         itemTypeIndex = Dice.getInt(Game.GetCountItemTypes(), 1) - 1;

         inventory.addItem(new Item(Game.getItemTypes().get(itemTypeIndex)));
      }
      this.bonus = inventory.calcBonus();
   }

   public Inventory getInventory() {
      return inventory;
   }

   public void dropInventory() {
      inventory.dropAllItems(location);
      this.bonus = inventory.calcBonus();
   }

   public void dropItem(Long itemID) {
      inventory.dropItem(itemID, location);
      this.bonus = inventory.calcBonus();
   }

   public void destroyItem(Long itemID) {
      inventory.destroyItem(itemID);
      this.bonus = inventory.calcBonus();
   }

   public void pickUpInventory() {
      inventory.pickUpItem(inventoryId);
      this.bonus = inventory.calcBonus();
   }

   public void decAttackDelay() {
      currentAttackDelay = Math.max(0, --currentAttackDelay);
   }

   public void move() {
      if (timeToRefresh == 0) {
         direction = Dice.getDirection();
         timeToRefresh = Dice.getInt(2, 250);
         if (clever && way == null)
            findParth();
      }
      timeToRefresh--;
      if (!clever && (aim == null || !aim.isLive())) {
         findAim();
      }
      if (!attack(aim)) {
         Direction moveTo;
         double step;
         if (clever && way != null && way.size() != 0 && lenWay >= 0){
            double dx = way.get(lenWay).x - this.location.x;
            double dy = way.get(lenWay).y - this.location.y;
            moveTo = Math.abs(dx) > Math.abs(dy)
                  ? dx > 0
                     ? Direction.EAST
                     : Direction.WEST
                  : dy > 0
                     ? Direction.SOUTH
                     : Direction.NORTH;
            step = Math.max(Math.abs(dx), Math.abs(dy));
            step = Math.min(step, getSpeed());
         } else {
            step = getSpeed();
            moveTo = aim == null
                  ? direction
                  : Dice.getBool(1)
                        ? aim.location.x - location.x < 0 ? Direction.WEST : Direction.EAST
                        : aim.location.y - location.y < 0 ? Direction.NORTH : Direction.SOUTH;
         }
         Game.unsetMonsterInLocation(location);
         if (!this.location.move(moveTo, step)) {
            direction = Dice.getDirection();
            findParth();
         }
         Game.setMonsterInLocation(this);
         if (clever && way != null && way.size() != 0 && lenWay >= 0
               && Math.abs(this.location.x - way.get(lenWay).x) < Location.eps
               && Math.abs(this.location.y - way.get(lenWay).y) < Location.eps){
            lenWay--;
         }
         if (clever && way != null && way.size() - lenWay > 3 || lenWay < 0){
            findParth();
         }
      }
   }

   private void findParth(){
      way = new ArrayList<>();
      boolean[][] useCells = new boolean[GameMap.getHeight()][GameMap.getWidth()];
      for (int i = 0; i < GameMap.getHeight(); i++)
         for (int j = 0; j < GameMap.getWidth(); j++)
            useCells[i][j] = false;
      PriorityBlockingQueue<CellOfFindMap> open = new PriorityBlockingQueue();
      open.add(new CellOfFindMap((int) this.location.x, (int) this.location.y, 0, null));
      while (!open.isEmpty()){
         CellOfFindMap cell = open.poll();
         if (!GameMap.canEnterTile(cell.x, cell.y)
               || useCells[cell.y][cell.x]
               || cell.distance > this.alertness) continue;
         useCells[cell.y][cell.x] = true;
         aim = Game.getActors(cell.x, cell.y);
         if (aim != null && isHate(aim)){
            while (cell != null){
               way.add(new Location(cell.x + 0.5, cell.y + 0.5));
               cell = cell.parent;
            }
            break;
         }
         open.add(new CellOfFindMap(cell.x - 1, cell.y, cell.distance + 1, cell));
         open.add(new CellOfFindMap(cell.x, cell.y - 1, cell.distance + 1, cell));
         open.add(new CellOfFindMap(cell.x + 1, cell.y, cell.distance + 1, cell));
         open.add(new CellOfFindMap(cell.x, cell.y + 1, cell.distance + 1, cell));
      }
      if (way.size() != 0){
         lenWay = way.size() - 1;
         if(Math.abs(this.location.x - way.get(lenWay).x) < Location.eps
         && Math.abs(this.location.y - way.get(lenWay).y) < Location.eps){
            way.remove(way.get(lenWay));
            lenWay--;
         }
      }
   }

   public void findAim() {
      aim = null;
      for (int i = -alertness; i <= alertness; i++)
         for (int j = -alertness; j <= alertness; j++) {
            Monster monster = Game.getActors((int) location.x + i, (int) location.y + j);
            if (monster != null && isHate(monster) && (aim == null || distance(monster.location) < distance(aim.location)))
               aim = monster;
         }
   }

   public void gotHit(double damage, Monster attacker) {
      stat.hp -= damage;
      if (attacker != null && !isLive()) {
         attacker.addExp(getExpKill());
      }
   }

   public boolean attack(Monster monster) {
      return playerAttack.attack(aim, this);
   }

   public double getDamage() {
      int damage = 0;
      if (canBlow) {
         for (Blow blow : blows) {
            damage += blow.getDamage();
         }
      }
      return damage * 5;
   }

   public boolean canAttack(Monster monster) {
      return monster != null && id != monster.id && currentAttackDelay == 0 && distance(monster.location) < 1.3;     //TODO 1 + расстояние атаки
   }

   private boolean isHate(Monster monster) {
      return hateThem == null || hateThem.size() == 0
            ? this.type != monster.type
            : hateThem.contains(monster.type);
   }

   public void addHater(Race race){
      hateThem.add(race);
   }

   private double distance(Location location) {
      return Math.sqrt(Math.pow(location.x - this.location.x, 2) + Math.pow(location.y - this.location.y, 2));
   }

   public JSONObject examine() {
      JSONObject result = new JSONObject();
      result.put("action", "examine");
      result.put("id", id);
      result.put("type", "monster");
      result.put("x", location.x);
      result.put("y", location.y);

      result.put("health", getHp());
      result.put("maxHealth", getMaxHp());
      result.put("mana", getMana());
      result.put("maxMana", getMaxMana());
      result.put("inventory", getInventory().inventoryToJSON());
      result.put("stats", stat.statsToJSON());


      result.put("name", name); //TODO Remove field "name" in client
      result.put("login", name);
      result.put("race", Race.raceToStr.get(type));
      result.put("result", "ok");
      return result;
   }

   public Race getType() {
      return type;
   }

   public double getHp() {
      return stat.hp;
   }

   public double getMana() {
      return stat.mana;
   }

   public Stat getStat() {
      return stat;
   }

   public int getExpKill() {
      return expKill;
   }

   public Level getLevel() {
      return level;
   }

   public double getSpeed() {
      return canMove
            ? stat.speed
            : 0;
   }

   public double getMaxHp() {
      return stat.maxHp;
   }

   public double getMaxMana() {
      return stat.maxMana;
   }

   public double getRegenHp() {
      return stat.regenHp;
   }

   public double getRegenMana() {
      return stat.regenMana;
   }

   public double getAttackDelay() {
      return stat.attackDelay;
   }

   public int getStrength() {
      return stat.strength;
   }

   public int getAgility() {
      return stat.agility;
   }

   public int getIntelligence() {
      return stat.intelligence;
   }

   public void addExp(int expKill) {
      return;
   }

   public void setCanMove(boolean canMove) {
      this.canMove = canMove;
   }

   public void setCanBlow(boolean canBlow) {
      this.canBlow = canBlow;
   }

   public void setClever(boolean clever) {
      this.clever = clever;
   }

   public void regenHpAndMana() {
      stat.hp = Math.max(0, Math.min(getMaxHp(), stat.hp + getRegenHp()));
      stat.mana = Math.max(0, Math.min(getMaxMana(), stat.hp + getRegenMana()));
   }

   public boolean isLive() {
      return getHp() > 0;
   }

   public void setInventoryId(long inventoryId) {
      this.inventoryId = inventoryId;
   }

   public long getInventoryId() {
      return inventoryId;
   }

   public PlayerAttack getPlayerAttack() {
      return playerAttack;
   }
}

