package MonsterQuest;

import java.util.Comparator;

/**
 * Created by Alexander on 7/13/14.
 */
public class CellOfFindMap implements Comparator<CellOfFindMap>, Comparable<CellOfFindMap>{
   public int x;
   public int y;
   public int distance;
   public CellOfFindMap parent;

   public CellOfFindMap(int x, int y, int distance, CellOfFindMap parent) {
      this.x = x;
      this.y = y;
      this.distance = distance;
      this.parent = parent;
   }

   @Override
   public int compare(CellOfFindMap c, CellOfFindMap c2){
      return c.distance - c.distance;
   }

   @Override
   public int compareTo(CellOfFindMap c) {
      return ((Integer)this.distance).compareTo(c.distance);
   }
}
