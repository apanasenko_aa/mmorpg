package MonsterQuest;

import org.json.simple.JSONObject;

/**
 * Created by razoriii on 27.05.14.
 */
public class Stat {
   public int strength;
   public int agility;
   public int intelligence;
   public double speed;
   public int defense;
   public int magic_resistance;
   public int capacity;
   public double maxHp;
   public double maxMana;
   public double hp;
   public double mana;

   public double damage;
   public double regenHp;
   public double regenMana;
   public double attackDelay;

   public Stat() {
      this.maxHp = 0;
      this.maxMana = 0;
      this.speed = 0;
      this.damage = 0;
      this.regenHp = 0;
      this.regenMana = 0;
      this.attackDelay = 0;
      this.agility = 0;
      this.strength = 0;
      this.intelligence = 0;
      this.defense = 0;
      this.capacity = 0;
      this.magic_resistance = 0;
      this.hp = 0;
      this.mana = 0;
   }

   public JSONObject statsToJSON() {
      JSONObject result = new JSONObject();
      result.put("strength", strength);
      result.put("agility", agility);
      result.put("intelligence", intelligence);
      result.put("speed", speed);
      result.put("defense", defense);
      result.put("magic_resistance", magic_resistance);
      result.put("capacity", capacity);
      result.put("hp", hp);
      result.put("maxHp", maxHp);
      result.put("mana", mana);
      result.put("maxMana", maxMana);
      return result;
   }

   public void setStat(
         int strength,
         int intelligence,
         int agility,
         double speed,
         int defense,
         int magic_resistance,
         int capacity,
         double maxHp,
         double maxMana,
         double hp,
         double mana) {
      this.strength = strength;
      this.intelligence = intelligence;
      this.agility = agility;
      this.speed = speed;
      this.defense = defense;
      this.magic_resistance = magic_resistance;
      this.capacity = capacity;
      this.maxHp = maxHp;
      this.maxMana = maxMana;
      this.hp = hp;
      this.mana = mana;
   }
}
