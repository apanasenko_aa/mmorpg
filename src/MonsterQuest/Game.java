package MonsterQuest;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.ConcurrentHashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.websocket.CloseReason;
import javax.websocket.Session;

public class Game {

   // TODO Убрать static (может быть много карт)

   private static final AtomicInteger globalId = new AtomicInteger(0);

   private static final HashMap<String, Long> playerIdsBySid = new HashMap<>();

   private static final ConcurrentHashMap<Long, Player> players = new ConcurrentHashMap<>();

   private static final ConcurrentHashMap<Long, Monster> monsters = new ConcurrentHashMap<>();

   private static final ConcurrentHashMap<Long, Projectiles> projectiles = new ConcurrentHashMap<>();

   private static final ConcurrentHashMap<Long, Item> items = new ConcurrentHashMap<>();

   private static final ArrayList<MonsterDB> monsterTypes = new ArrayList<>();

   private static final ArrayList<ItemDB> itemTypes = new ArrayList<>();

   private static final ArrayList<SpawnPoint> spawnPoints = new ArrayList<>();

   private static Timer gameTimer = null;

   public static double ticksPerSecond = 100;

   public static double defaultSpeed = 0.07;

   private static long tickValue = 1;

   private static boolean started = false;

   private static int saveToDBTick = 1;

   private static final int DB_SAVE_DELAY = 20;

   private static double pickUpRadius = 5;

   public static double getPickUpRadius() {
      return pickUpRadius;
   }

   public static void setPickUpRadius(double pickUpRadius) {
      Game.pickUpRadius = pickUpRadius;
   }

   private static final long TICK_DELAY = 10;

   private static Monster[][] actorsMap;

   private static List<List<List<Projectiles>>> projectilesMap;

   private static List<List<List<Item>>> itemsMap;

   private static void initializeActorsMap(int height, int width) {
      actorsMap = new Monster[height][];
      for (int i = 0; i < height; i++) {
         Monster[] line = new Monster[width];
         Arrays.fill(line, null);
         actorsMap[i] = line;
      }
   }

   public static void setMonsterInLocation(Monster monster) {
      actorsMap[(int) monster.getLocation().y][(int) monster.getLocation().x] = monster;
   }

   public static void unsetMonsterInLocation(Location location) {
      if (location.x < actorsMap[0].length && location.y < actorsMap.length)
         actorsMap[(int) location.y][(int) location.x] = null;
   }

   public static Monster getActors(int x, int y) {
      return x >= 0 && x < GameMap.getWidth()
            && y >= 0 && y < GameMap.getHeight()
            ? actorsMap[y][x] : null;
   }

   //_____________________Session________________________//

   private static final ArrayList<Session> sessions = new ArrayList<>();

   public static void addSession(Session session) {
      sessions.add(session);
   }

   public static void removeSession(Session session) {
      sessions.remove(session);
   }

   //__________________Projectiles_map___________________//

   private static void initializeProjectilesMap(int height, int width) {
      projectilesMap = new ArrayList<List<List<Projectiles>>>();
      for (int i = 0; i < height; i++) {
         projectilesMap.add(new ArrayList<List<Projectiles>>());
         for (int j = 0; j < width; j++)
            projectilesMap.get(i).add(new ArrayList<Projectiles>());
      }
   }

   public static void setProjectilesInLocation(Projectiles projectiles) {
      projectilesMap.get((int) projectiles.getLocation().y).get((int) projectiles.getLocation().x).add(projectiles);
   }

   public static void unsetProjectilesInLocation(Projectiles projectiles) {
      for (Projectiles s : projectilesMap.get((int) projectiles.getLocation().y).get((int) projectiles.getLocation().x))
         if (s.getId() == projectiles.getId()) {
            projectilesMap.get((int) projectiles.getLocation().y).get((int) projectiles.getLocation().x).remove(s);
            break;
         }
   }

   public static List<Projectiles> getProjectiles(int x, int y) {
      return x > 0 && x < GameMap.getWidth()
            && y > 0 && y < GameMap.getHeight()
            ? projectilesMap.get(y).get(x) : new ArrayList<Projectiles>(0);
   }

   public static void addProjectiles(Projectiles projectile) {
      Game.setProjectilesInLocation(projectile);
      projectiles.put(projectile.getId(), projectile);
   }

   private static void removeProjectiles(Projectiles projectile) {
      unsetProjectilesInLocation(projectile);
      projectiles.remove(projectile.getId());
   }

   protected static Collection<Projectiles> getProjectiles() {
      return Collections.unmodifiableCollection(projectiles.values());
   }

   //__________________Items_map___________________//

   private static void initializeItemsMap(int height, int width) {
      itemsMap = new ArrayList<List<List<Item>>>();
      for (int i = 0; i < height; i++) {
         itemsMap.add(new ArrayList<List<Item>>());
         for (int j = 0; j < width; j++)
            itemsMap.get(i).add(new ArrayList<Item>());
      }
   }

   public static void setItemInLocation(Item item) {
      itemsMap.get((int) item.getLocation().y).get((int) item.getLocation().x).add(item);
   }

   public static void unsetItemsInLocation(Item item) {
      for (Item s : itemsMap.get((int) item.getLocation().y).get((int) item.getLocation().x))
         if (s.getId() == item.getId()) {
            itemsMap.get((int) item.getLocation().y).get((int) item.getLocation().x).remove(s);
            break;
         }
   }

   public static List<Item> getItems(int x, int y) {
      return x > 0 && x < GameMap.getWidth()
            && y > 0 && y < GameMap.getHeight()
            ? itemsMap.get(y).get(x) : new ArrayList<Item>(0);
   }

   public static void addItem(Item item) {
      Game.setItemInLocation(item);
      items.put(item.getId(), item);
   }

   public static void removeItems(Item item) {
      unsetItemsInLocation(item);
      items.remove(item.getId());
   }

   protected static Item getItem(Long id) {
      return items.get(id);
   }

   protected static synchronized void addPlayer(Player player) {
      if (!player.location.isLocationValid()) {
         player.location.x %= GameMap.getWidth();
         player.location.x = Math.round(player.location.x) + 0.5;
         player.location.y %= GameMap.getHeight();
         player.location.y = Math.round(player.location.y) + 0.5;
         player.location.getFreeLocation();
      }
      Game.setMonsterInLocation(player);
      players.put(player.getId(), player);
   }

   protected static synchronized void addMonster(Monster monster) {
      Game.setMonsterInLocation(monster);
      monsters.put(monster.getId(), monster);
   }

   protected static synchronized void addSpawnPoint(SpawnPoint spawnPoint) {
      spawnPoints.add(spawnPoint);
   }

   protected static Monster createMonster(MonsterDB monsterType, Location location) {
      return new Monster
            (getNextGlobalId()
                  , monsterType.getName()
                  , monsterType.getType()
                  , monsterType.getHp()
                  , monsterType.getMana()
                  , monsterType.getRegenHp()
                  , monsterType.getRegenMana()
                  , monsterType.getAttackDelay()
                  , monsterType.getExpKill()
                  , monsterType.getAlertness()
                  , monsterType.getSpeed()
                  , monsterType.getBlows()
                  , monsterType.getFlags()
                  , location.getFreeLocation()
                  , true
            );
   }

   protected static Monster createStandartMonster
         ( String name
         , Race race
         , boolean canMove
         , boolean canBlow
         , Blow blow
         , double x
         , double y
         ) {
      ArrayList<Blow>blows = new ArrayList<Blow>();
      blows.add(blow);
      Monster monster = new Monster(getNextGlobalId()
                                    , name
                                    , race
                                    , 100
                                    , 100
                                    , 0
                                    , 0
                                    , 15
                                    , 100
                                    , 10
                                    , 0.2
                                    , blows
                                    , new ArrayList<Flag>()
                                    , new Location(x, y)
                                    , true
                                    );
      monster.setCanMove(canMove);
      monster.setCanBlow(canBlow);
      monster.setClever(false);
      return monster;
   }

   protected static Player findPlayerBySid(String sid) {
      for (Player player : getPlayers()) {
         if (player.getSid().equals(sid)) {
            return player;
         }
      }
      return null;
   }

   protected static Collection<Player> getPlayers() {
      return Collections.unmodifiableCollection(players.values());
   }

   protected static Collection<Monster> getMonsters() {
      return Collections.unmodifiableCollection(monsters.values());
   }

   protected static ArrayList<MonsterDB> getMonsterTypes() {
      return monsterTypes;
   }

   protected static ArrayList<ItemDB> getItemTypes() {
      return itemTypes;
   }

   protected static JSONArray getActorsList(Location location) {
      JSONArray jsonAns = new JSONArray();
      for (int j = -GameMap.SIGHT_RADIUS_Y; j < GameMap.SIGHT_RADIUS_Y; j++)
         for (int i = -GameMap.SIGHT_RADIUS_X; i < GameMap.SIGHT_RADIUS_X; i++) {
            Monster monster = Game.getActors((int) location.x - i, (int) location.y - j);
            if (monster != null) {
               JSONObject jsonActor = new JSONObject();
               jsonActor.put("id", monster.getId());
               jsonActor.put("x", monster.getLocation().x);
               jsonActor.put("y", monster.getLocation().y);

               jsonActor.put("health", monster.getHp());
               jsonActor.put("maxHealth", monster.getMaxHp());
               jsonActor.put("mana", monster.getMana());
               jsonActor.put("maxMana", monster.getMaxMana());

               if (monster.getType() == Race.PLAYER) {
                  jsonActor.put("type", "player");
                  jsonActor.put("name", monster.getName());
                  jsonActor.put("class", PlayerClassType.toString(((Player) monster).getPlayerClass().getClassType())); //TODO Make class by protocol
               } else {
                  jsonActor.put("type", "monster");
                  jsonActor.put("name", monster.getName());
                  jsonActor.put("race", Race.raceToStr.get(monster.getType()));
               }
               jsonAns.add(jsonActor);
            }
            for (Projectiles projectiles : Game.getProjectiles((int) location.x - i, (int) location.y - j)) {
               JSONObject jsonActor = new JSONObject();
               jsonActor.put("type", "projectile");
               jsonActor.put("name", "fireball_projectile"); //TODO Make names by protocol
               jsonActor.put("id", projectiles.getId());
               jsonActor.put("x", projectiles.getLocation().x);
               jsonActor.put("y", projectiles.getLocation().y);
               jsonAns.add(jsonActor);
            }
            for (Item item : Game.getItems((int) location.x - i, (int) location.y - j)) {
               JSONObject jsonActor = new JSONObject();
               jsonActor.put("type", "item");
               jsonActor.put("name", item.getName());
               jsonActor.put("itemType", item.getType());
               jsonActor.put("id", item.getId());
               jsonActor.put("x", item.getLocation().x);
               jsonActor.put("y", item.getLocation().y);
               jsonAns.add(jsonActor);
            }
         }
      return jsonAns;
   }

   protected static Player examinePlayer(long playerId) {
      return players.get(playerId);
   }

   protected static Monster examineMonster(long monsterId) {
      return monsters.get(monsterId);
   }

   protected static Item examineItem(long itemId) {
      return items.get(itemId);
   }

   protected static synchronized void removePlayer(Player player) {
      players.remove(player.getId());
   }

   protected static synchronized void removeMonster(Monster monster) {
      unsetMonsterInLocation(monster.location);
      monsters.remove(monster.getId());
   }

   protected static void tick() {
      JSONObject jsonAns = new JSONObject();
      tickValue++;
      saveToDBTick++;
      jsonAns.put("tick", tickValue);
      if (saveToDBTick >= DB_SAVE_DELAY) {
         for (Player player : getPlayers()) {
            player.saveStateToBD();
         }
         saveToDBTick = 0;
      }

      for (SpawnPoint spawnPoint : spawnPoints) {
         if (Dice.getBool(7)) {
            spawnPoint.spawnMonster();
         }
      }

      for (Player player : getPlayers()) {
         player.decAttackDelay();
         if (!PlayerAnnotation.isTest()) {
            player.regenHpAndMana();
         }
         player.move();
      }

      for (Monster monster : getMonsters()) {

         if (monster.isLive()) {
            monster.decAttackDelay();
            if (!PlayerAnnotation.isTest()) {
               monster.regenHpAndMana();
            }
            monster.move();
         } else {
            JSONObject message = new JSONObject();
            message.put("event", monster.getId());
            monster.dropInventory();
            Game.removeMonster(monster);
            broadcast(message);
         }
      }

      for (Projectiles projectiles : getProjectiles()) {
         projectiles.move();
      }

      for (Projectiles projectiles : getProjectiles()) {
         if (projectiles.mustBang()) {
            removeProjectiles(projectiles);
            projectiles.bang();
         }
      }

      broadcast(jsonAns);
   }

   protected static void broadcast(JSONObject message) {
      //System.out.println(sessions.size());
      for (Session session : sessions) {
         try {
            //System.out.println(session.isOpen());
            if (session.isOpen())
               //System.out.println(message);
               session.getBasicRemote().sendText(message.toJSONString());
         } catch (IOException ioe) {
            CloseReason cr =
                  new CloseReason(CloseReason.CloseCodes.CLOSED_ABNORMALLY, ioe.getMessage());
            try {
               session.close(cr);
            } catch (IOException ioe2) {
               // Ignore
            }
         }
      }
   }

   private static void loadMonsterTypes() {
      for (MonsterDB monsterDB : MonsterDB.loadMonstersFromDB()) {
         monsterTypes.add(monsterDB);
      }
   }

   private static void loadItemTypes() {
      for (ItemDB itemDB : ItemDB.loadItemFromDB()) {
         itemTypes.add(itemDB);
      }
   }


   public static void startTimer() {
      if (started) return;
      started = true;
      GameMap.saveToBdDemoMap();
      GameMap.loadWorldMap();
      GameDictionary.loadDictionary();
      Game.loadMonsterTypes();
      Game.loadItemTypes();
      initializeActorsMap(GameMap.getHeight(), GameMap.getWidth());
      initializeProjectilesMap(GameMap.getHeight(), GameMap.getWidth());
      initializeItemsMap(GameMap.getHeight(), GameMap.getWidth());
      addSpawnPoint(new SpawnPoint(new Location(13, 6), 800));
//      addSpawnPoint(new SpawnPoint(new Location(13, 12), 8));
//      addSpawnPoint(new SpawnPoint(new Location(13, 15), 20));
//      addSpawnPoint(new SpawnPoint(new Location(13, 25), 20)); // TODO Новые точки нужно хранить на карте (или в базе) а не вставлять их копипастом
//      addSpawnPoint(new SpawnPoint(new Location(20, 25), 100)); // TODO много точек с разной глубиной
      gameTimer = new Timer(Game.class.getSimpleName() + " Timer");
      gameTimer.scheduleAtFixedRate(new TimerTask() {
         @Override
         public void run() {
            try {
               tick();
            } catch (RuntimeException e) {
               System.out.print("ERROR!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
               System.out.print(e.getLocalizedMessage());
            }
         }
      }, TICK_DELAY, TICK_DELAY);
   }

   public static int GetCountItemTypes() {
      return itemTypes.size();
   }

   public static int GetCountMonsterTypes() {
      return monsterTypes.size();
   }

   public static void resetGame(JSONArray map, Player player) throws Throwable {
      if (map.size() == 0 || ((JSONArray)map.toArray()[0]).toArray().length == 0)
         throw new Throwable();
      for (int i = 1; i < map.size(); i++){
         if (((JSONArray)map.toArray()[0]).toArray().length != ((JSONArray)map.toArray()[i]).toArray().length){
            throw new Throwable();
         }
      }
      started = true;
      monsters.clear();
      projectiles.clear();
      spawnPoints.clear();
      items.clear();
      GameMap.setUpMapFromJson(map);
      playerIdsBySid.clear();
      players.clear();
      initializeActorsMap(GameMap.getHeight(), GameMap.getWidth());
      initializeProjectilesMap(GameMap.getHeight(), GameMap.getWidth());
      initializeItemsMap(GameMap.getHeight(), GameMap.getWidth());
      //addPlayer(player);
      setPlayerIdBySid(player.getSid(), player.getId());
      players.put(player.getId(), player);
   }

   public static synchronized long getNextGlobalId() {
      return globalId.getAndIncrement();
   }

   public static long getPlayerIdBySid(String sid) {
      return playerIdsBySid.get(sid);
   }

   public static void setPlayerIdBySid(String sid, long id) {
      playerIdsBySid.put(sid, id);
   }

}



