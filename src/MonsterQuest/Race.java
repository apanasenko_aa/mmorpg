package MonsterQuest;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Alexander on 7/9/14.
 */
public enum Race {
   ORC,
   EVIL,
   TROLL,
   GIANT,
   DEMON,
   METAL,
   DRAGON,
   UNDEAD,
   ANIMAL,
   PLAYER;

   public static final ConcurrentHashMap<String, Race> strToRace = new ConcurrentHashMap(){{
      put("EVIL", EVIL);
      put("TROLL", TROLL);
      put("GIANT", GIANT);
      put("ORC", ORC);
      put("DEMON", DEMON);
      put("METAL", METAL);
      put("DRAGON", DRAGON);
      put("UNDEAD", UNDEAD);
      put("ANIMAL", ANIMAL);
      put("player", PLAYER);
      put("PLAYER", PLAYER);
   }};

   public static final ConcurrentHashMap<Race, String> raceToStr = new ConcurrentHashMap(){{
      put(EVIL, "EVIL");
      put(TROLL, "TROLL");
      put(GIANT, "GIANT");
      put(ORC, "ORC");
      put(DEMON, "DEMON");
      put(METAL, "METAL");
      put(DRAGON, "DRAGON");
      put(UNDEAD, "UNDEAD");
      put(ANIMAL, "ANIMAL");
      put(PLAYER, "player");
   }};

   public static final ConcurrentHashMap<String, Race> hateIs = new ConcurrentHashMap(){{
      put("HATE_EVIL", EVIL);
      put("HATE_TROLL", TROLL);
      put("HATE_GIANT", GIANT);
      put("HATE_ORC", ORC);
      put("HATE_DEMON", DEMON);
      put("HATE_METAL", METAL);
      put("HATE_DRAGON", DRAGON);
      put("HATE_UNDEAD", UNDEAD);
      put("HATE_ANIMAL", ANIMAL);
      put("HATE_PLAYER", PLAYER);
   }};

}
