package MonsterQuest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Created by razoriii on 28.05.14.
 */
public class PlayerAttack {
   private int attackId;

   public PlayerAttack() {}

   public boolean attack(Monster aim, Monster attacker) {
      switch (attackId) {
         case 1: {
            if (attacker.canAttack(aim)) {
               aim.gotHit(attacker.getDamage(), attacker);
               if (aim.getType() != Race.PLAYER) {
                  aim.aim = attacker;
               }
               attacker.currentAttackDelay = attacker.getAttackDelay();
               return true;
            }
            return false;
         }
         case 2: {
            if ((attacker.aimX != 0 || attacker.aimY != 0) && (attacker.stat.mana >= 2 * attacker.getLevel().calcLevel())) { //TODO - 50 - кол-во маны за фаерболл
               attacker.stat.mana -= 2 * attacker.getLevel().calcLevel();
               Game.addProjectiles(new Projectiles(AttackMethod.FIREBALL
                       , attacker.location
                       , 0.5
                       , attacker.aimX - attacker.location.x
                       , attacker.aimY - attacker.location.y
                       , 0.2
                       , 2.0
                       , attacker.getDamage()
                       , attacker
               ));
               attacker.aimX = 0;
               attacker.aimY = 0;
               return true;
            }
            return false;

         }
         case 3: {

         }
      }
      return false;
   }

   public void setAttackId(int attackId) {
      this.attackId = attackId;
   }
}
